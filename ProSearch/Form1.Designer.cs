﻿namespace ProSearch
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.cb_sale_office = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cb_district = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cb_province = new System.Windows.Forms.ComboBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.tb_contact_count = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.cb_chon_so = new System.Windows.Forms.ComboBox();
            this.tb_delay_chon_so = new System.Windows.Forms.TextBox();
            this.ck_chon_so = new System.Windows.Forms.CheckBox();
            this.label18 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lb_is_token_alive = new System.Windows.Forms.Label();
            this.btn_logout = new System.Windows.Forms.Button();
            this.btn_login = new System.Windows.Forms.Button();
            this.ck_xac_thuc_tap_doan = new System.Windows.Forms.CheckBox();
            this.tb_employee_password = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.tb_employee_account = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tb_bo0 = new System.Windows.Forms.TextBox();
            this.tb_bo53 = new System.Windows.Forms.TextBox();
            this.tb_bo7 = new System.Windows.Forms.TextBox();
            this.tb_bo4 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.ck_bo_0 = new System.Windows.Forms.CheckBox();
            this.ck_bo_53 = new System.Windows.Forms.CheckBox();
            this.ck_bo_7 = new System.Windows.Forms.CheckBox();
            this.ck_bo4 = new System.Windows.Forms.CheckBox();
            this.tb_found = new System.Windows.Forms.TextBox();
            this.tb_result = new System.Windows.Forms.TextBox();
            this.tb_log = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.btn_pause = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tb_delay = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tb_dang_so = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cb_loai_tb = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tb_lien_he = new System.Windows.Forms.TextBox();
            this.lb_contact = new System.Windows.Forms.Label();
            this.tb_password = new System.Windows.Forms.TextBox();
            this.tb_username = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_start = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox5);
            this.tabPage1.Controls.Add(this.groupBox6);
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.tb_found);
            this.tabPage1.Controls.Add(this.tb_result);
            this.tabPage1.Controls.Add(this.tb_log);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.btn_pause);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.btn_start);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1212, 628);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Dashboard";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.button1);
            this.groupBox5.Controls.Add(this.cb_sale_office);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.cb_district);
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Controls.Add(this.cb_province);
            this.groupBox5.Location = new System.Drawing.Point(252, 149);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(267, 146);
            this.groupBox5.TabIndex = 33;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Cấu hình địa chỉ (only PGD)";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(67, 105);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(133, 23);
            this.button1.TabIndex = 34;
            this.button1.Text = "Login";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // cb_sale_office
            // 
            this.cb_sale_office.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_sale_office.FormattingEnabled = true;
            this.cb_sale_office.Location = new System.Drawing.Point(93, 72);
            this.cb_sale_office.Name = "cb_sale_office";
            this.cb_sale_office.Size = new System.Drawing.Size(168, 21);
            this.cb_sale_office.TabIndex = 26;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 75);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(84, 13);
            this.label9.TabIndex = 25;
            this.label9.Text = "Phòng giao dịch";
            // 
            // cb_district
            // 
            this.cb_district.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_district.FormattingEnabled = true;
            this.cb_district.Location = new System.Drawing.Point(93, 45);
            this.cb_district.Name = "cb_district";
            this.cb_district.Size = new System.Drawing.Size(168, 21);
            this.cb_district.TabIndex = 24;
            this.cb_district.SelectedIndexChanged += new System.EventHandler(this.cb_district_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 49);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 13);
            this.label7.TabIndex = 23;
            this.label7.Text = "Quận / Huyện";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(28, 13);
            this.label6.TabIndex = 22;
            this.label6.Text = "Tỉnh";
            // 
            // cb_province
            // 
            this.cb_province.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_province.FormattingEnabled = true;
            this.cb_province.Location = new System.Drawing.Point(93, 19);
            this.cb_province.Name = "cb_province";
            this.cb_province.Size = new System.Drawing.Size(168, 21);
            this.cb_province.TabIndex = 0;
            this.cb_province.SelectedIndexChanged += new System.EventHandler(this.cb_province_SelectedIndexChanged);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.tb_contact_count);
            this.groupBox6.Controls.Add(this.label10);
            this.groupBox6.Controls.Add(this.textBox1);
            this.groupBox6.Controls.Add(this.label20);
            this.groupBox6.Controls.Add(this.cb_chon_so);
            this.groupBox6.Controls.Add(this.tb_delay_chon_so);
            this.groupBox6.Controls.Add(this.ck_chon_so);
            this.groupBox6.Controls.Add(this.label18);
            this.groupBox6.Location = new System.Drawing.Point(252, 7);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(267, 136);
            this.groupBox6.TabIndex = 32;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Cấu hình chọn số";
            // 
            // tb_contact_count
            // 
            this.tb_contact_count.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tb_contact_count.Location = new System.Drawing.Point(185, 103);
            this.tb_contact_count.Name = "tb_contact_count";
            this.tb_contact_count.Size = new System.Drawing.Size(53, 20);
            this.tb_contact_count.TabIndex = 33;
            this.tb_contact_count.Text = "3";
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 97);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(136, 26);
            this.label10.TabIndex = 32;
            this.label10.Text = "Lượt chọn / 1 liên hệ (digi)\r\nĐể trống nếu auto 1 liên hệ";
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textBox1.Location = new System.Drawing.Point(154, 48);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(84, 20);
            this.textBox1.TabIndex = 31;
            this.textBox1.Text = "12000";
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 51);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(123, 13);
            this.label20.TabIndex = 30;
            this.label20.Text = "Delay chọn số (digishop)";
            // 
            // cb_chon_so
            // 
            this.cb_chon_so.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_chon_so.FormattingEnabled = true;
            this.cb_chon_so.Items.AddRange(new object[] {
            "0. Digishop",
            "1. Employee"});
            this.cb_chon_so.Location = new System.Drawing.Point(131, 19);
            this.cb_chon_so.Name = "cb_chon_so";
            this.cb_chon_so.Size = new System.Drawing.Size(107, 21);
            this.cb_chon_so.TabIndex = 27;
            // 
            // tb_delay_chon_so
            // 
            this.tb_delay_chon_so.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tb_delay_chon_so.Location = new System.Drawing.Point(154, 74);
            this.tb_delay_chon_so.Name = "tb_delay_chon_so";
            this.tb_delay_chon_so.Size = new System.Drawing.Size(84, 20);
            this.tb_delay_chon_so.TabIndex = 29;
            this.tb_delay_chon_so.Text = "12000";
            // 
            // ck_chon_so
            // 
            this.ck_chon_so.AutoSize = true;
            this.ck_chon_so.Location = new System.Drawing.Point(11, 23);
            this.ck_chon_so.Name = "ck_chon_so";
            this.ck_chon_so.Size = new System.Drawing.Size(65, 17);
            this.ck_chon_so.TabIndex = 14;
            this.ck_chon_so.Text = "Chọn số";
            this.ck_chon_so.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 77);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(129, 13);
            this.label18.TabIndex = 28;
            this.label18.Text = "Delay chọn số (employee)";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lb_is_token_alive);
            this.groupBox4.Controls.Add(this.btn_logout);
            this.groupBox4.Controls.Add(this.btn_login);
            this.groupBox4.Controls.Add(this.ck_xac_thuc_tap_doan);
            this.groupBox4.Controls.Add(this.tb_employee_password);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.tb_employee_account);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Location = new System.Drawing.Point(11, 462);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(237, 152);
            this.groupBox4.TabIndex = 26;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Employee account";
            // 
            // lb_is_token_alive
            // 
            this.lb_is_token_alive.AutoSize = true;
            this.lb_is_token_alive.Location = new System.Drawing.Point(98, 120);
            this.lb_is_token_alive.Name = "lb_is_token_alive";
            this.lb_is_token_alive.Size = new System.Drawing.Size(0, 13);
            this.lb_is_token_alive.TabIndex = 30;
            // 
            // btn_logout
            // 
            this.btn_logout.Location = new System.Drawing.Point(115, 94);
            this.btn_logout.Name = "btn_logout";
            this.btn_logout.Size = new System.Drawing.Size(51, 23);
            this.btn_logout.TabIndex = 29;
            this.btn_logout.Text = "Logout";
            this.btn_logout.UseVisualStyleBackColor = true;
            this.btn_logout.Click += new System.EventHandler(this.btn_logout_Click);
            // 
            // btn_login
            // 
            this.btn_login.Location = new System.Drawing.Point(56, 94);
            this.btn_login.Name = "btn_login";
            this.btn_login.Size = new System.Drawing.Size(53, 23);
            this.btn_login.TabIndex = 28;
            this.btn_login.Text = "Login";
            this.btn_login.UseVisualStyleBackColor = true;
            this.btn_login.Click += new System.EventHandler(this.btn_login_Click);
            // 
            // ck_xac_thuc_tap_doan
            // 
            this.ck_xac_thuc_tap_doan.AutoSize = true;
            this.ck_xac_thuc_tap_doan.Location = new System.Drawing.Point(9, 71);
            this.ck_xac_thuc_tap_doan.Name = "ck_xac_thuc_tap_doan";
            this.ck_xac_thuc_tap_doan.Size = new System.Drawing.Size(190, 17);
            this.ck_xac_thuc_tap_doan.TabIndex = 27;
            this.ck_xac_thuc_tap_doan.Text = "Đăng nhập qua xác thực tập đoàn";
            this.ck_xac_thuc_tap_doan.UseVisualStyleBackColor = true;
            // 
            // tb_employee_password
            // 
            this.tb_employee_password.Location = new System.Drawing.Point(82, 45);
            this.tb_employee_password.Name = "tb_employee_password";
            this.tb_employee_password.Size = new System.Drawing.Size(149, 20);
            this.tb_employee_password.TabIndex = 7;
            this.tb_employee_password.Text = "b";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(10, 48);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 13);
            this.label16.TabIndex = 6;
            this.label16.Text = "Password";
            // 
            // tb_employee_account
            // 
            this.tb_employee_account.Location = new System.Drawing.Point(82, 19);
            this.tb_employee_account.Name = "tb_employee_account";
            this.tb_employee_account.Size = new System.Drawing.Size(149, 20);
            this.tb_employee_account.TabIndex = 5;
            this.tb_employee_account.Text = "a";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(10, 22);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(55, 13);
            this.label17.TabIndex = 4;
            this.label17.Text = "Username";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tb_bo0);
            this.groupBox3.Controls.Add(this.tb_bo53);
            this.groupBox3.Controls.Add(this.tb_bo7);
            this.groupBox3.Controls.Add(this.tb_bo4);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.ck_bo_0);
            this.groupBox3.Controls.Add(this.ck_bo_53);
            this.groupBox3.Controls.Add(this.ck_bo_7);
            this.groupBox3.Controls.Add(this.ck_bo4);
            this.groupBox3.Location = new System.Drawing.Point(11, 356);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(237, 100);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Cấu hình lọc";
            // 
            // tb_bo0
            // 
            this.tb_bo0.Location = new System.Drawing.Point(111, 62);
            this.tb_bo0.Name = "tb_bo0";
            this.tb_bo0.Size = new System.Drawing.Size(17, 20);
            this.tb_bo0.TabIndex = 23;
            this.tb_bo0.Text = "3";
            // 
            // tb_bo53
            // 
            this.tb_bo53.Location = new System.Drawing.Point(111, 36);
            this.tb_bo53.Name = "tb_bo53";
            this.tb_bo53.Size = new System.Drawing.Size(17, 20);
            this.tb_bo53.TabIndex = 22;
            this.tb_bo53.Text = "3";
            // 
            // tb_bo7
            // 
            this.tb_bo7.Location = new System.Drawing.Point(9, 62);
            this.tb_bo7.Name = "tb_bo7";
            this.tb_bo7.Size = new System.Drawing.Size(17, 20);
            this.tb_bo7.TabIndex = 21;
            this.tb_bo7.Text = "3";
            // 
            // tb_bo4
            // 
            this.tb_bo4.Location = new System.Drawing.Point(9, 36);
            this.tb_bo4.Name = "tb_bo4";
            this.tb_bo4.Size = new System.Drawing.Size(17, 20);
            this.tb_bo4.TabIndex = 20;
            this.tb_bo4.Text = "3";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 20);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(72, 13);
            this.label19.TabIndex = 18;
            this.label19.Text = "Lọc n số đuôi";
            // 
            // ck_bo_0
            // 
            this.ck_bo_0.AutoSize = true;
            this.ck_bo_0.Location = new System.Drawing.Point(134, 65);
            this.ck_bo_0.Name = "ck_bo_0";
            this.ck_bo_0.Size = new System.Drawing.Size(48, 17);
            this.ck_bo_0.TabIndex = 5;
            this.ck_bo_0.Text = "Bỏ 0";
            this.ck_bo_0.UseVisualStyleBackColor = true;
            // 
            // ck_bo_53
            // 
            this.ck_bo_53.AutoSize = true;
            this.ck_bo_53.Location = new System.Drawing.Point(134, 39);
            this.ck_bo_53.Name = "ck_bo_53";
            this.ck_bo_53.Size = new System.Drawing.Size(54, 17);
            this.ck_bo_53.TabIndex = 4;
            this.ck_bo_53.Text = "Bỏ 53";
            this.ck_bo_53.UseVisualStyleBackColor = true;
            // 
            // ck_bo_7
            // 
            this.ck_bo_7.AutoSize = true;
            this.ck_bo_7.Location = new System.Drawing.Point(32, 65);
            this.ck_bo_7.Name = "ck_bo_7";
            this.ck_bo_7.Size = new System.Drawing.Size(48, 17);
            this.ck_bo_7.TabIndex = 3;
            this.ck_bo_7.Text = "Bỏ 7";
            this.ck_bo_7.UseVisualStyleBackColor = true;
            // 
            // ck_bo4
            // 
            this.ck_bo4.AutoSize = true;
            this.ck_bo4.Location = new System.Drawing.Point(32, 39);
            this.ck_bo4.Name = "ck_bo4";
            this.ck_bo4.Size = new System.Drawing.Size(48, 17);
            this.ck_bo4.TabIndex = 2;
            this.ck_bo4.Text = "Bỏ 4";
            this.ck_bo4.UseVisualStyleBackColor = true;
            // 
            // tb_found
            // 
            this.tb_found.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_found.Location = new System.Drawing.Point(939, 22);
            this.tb_found.MaxLength = 992767000;
            this.tb_found.Multiline = true;
            this.tb_found.Name = "tb_found";
            this.tb_found.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_found.Size = new System.Drawing.Size(133, 542);
            this.tb_found.TabIndex = 10;
            // 
            // tb_result
            // 
            this.tb_result.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_result.Location = new System.Drawing.Point(1076, 22);
            this.tb_result.MaxLength = 992767000;
            this.tb_result.Multiline = true;
            this.tb_result.Name = "tb_result";
            this.tb_result.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_result.Size = new System.Drawing.Size(133, 542);
            this.tb_result.TabIndex = 0;
            // 
            // tb_log
            // 
            this.tb_log.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_log.Location = new System.Drawing.Point(536, 22);
            this.tb_log.MaxLength = 992767000;
            this.tb_log.Multiline = true;
            this.tb_log.Name = "tb_log";
            this.tb_log.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_log.Size = new System.Drawing.Size(397, 542);
            this.tb_log.TabIndex = 3;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(936, 6);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 13);
            this.label11.TabIndex = 11;
            this.label11.Text = "Đã tìm thấy";
            // 
            // btn_pause
            // 
            this.btn_pause.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_pause.Enabled = false;
            this.btn_pause.Location = new System.Drawing.Point(939, 599);
            this.btn_pause.Name = "btn_pause";
            this.btn_pause.Size = new System.Drawing.Size(133, 23);
            this.btn_pause.TabIndex = 7;
            this.btn_pause.Text = "Pause";
            this.btn_pause.UseVisualStyleBackColor = true;
            this.btn_pause.Click += new System.EventHandler(this.btn_pause_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tb_delay);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.tb_dang_so);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.cb_loai_tb);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Location = new System.Drawing.Point(252, 301);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(267, 235);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Cấu hình tìm kiếm";
            // 
            // tb_delay
            // 
            this.tb_delay.Location = new System.Drawing.Point(93, 16);
            this.tb_delay.Name = "tb_delay";
            this.tb_delay.Size = new System.Drawing.Size(168, 20);
            this.tb_delay.TabIndex = 5;
            this.tb_delay.Text = "1000";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 19);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(34, 13);
            this.label12.TabIndex = 4;
            this.label12.Text = "Delay";
            // 
            // tb_dang_so
            // 
            this.tb_dang_so.Location = new System.Drawing.Point(15, 90);
            this.tb_dang_so.Multiline = true;
            this.tb_dang_so.Name = "tb_dang_so";
            this.tb_dang_so.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_dang_so.Size = new System.Drawing.Size(246, 136);
            this.tb_dang_so.TabIndex = 17;
            this.tb_dang_so.Text = "8491*888\r\n8494*888\r\n8488*888\r\n8485*888\r\n8484*888\r\n8483*888\r\n8482*888\r\n8481*888";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 72);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(112, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Số | Dạng số (Đầu 84)";
            // 
            // cb_loai_tb
            // 
            this.cb_loai_tb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_loai_tb.FormattingEnabled = true;
            this.cb_loai_tb.Items.AddRange(new object[] {
            "1. Trả trước",
            "2. Trả sau"});
            this.cb_loai_tb.Location = new System.Drawing.Point(93, 42);
            this.cb_loai_tb.Name = "cb_loai_tb";
            this.cb_loai_tb.Size = new System.Drawing.Size(168, 21);
            this.cb_loai_tb.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 45);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Loại TB";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tb_lien_he);
            this.groupBox1.Controls.Add(this.lb_contact);
            this.groupBox1.Controls.Add(this.tb_password);
            this.groupBox1.Controls.Add(this.tb_username);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(8, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(238, 184);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Digi account";
            // 
            // tb_lien_he
            // 
            this.tb_lien_he.Location = new System.Drawing.Point(83, 72);
            this.tb_lien_he.Multiline = true;
            this.tb_lien_he.Name = "tb_lien_he";
            this.tb_lien_he.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_lien_he.Size = new System.Drawing.Size(149, 99);
            this.tb_lien_he.TabIndex = 21;
            this.tb_lien_he.Text = "0886881999";
            // 
            // lb_contact
            // 
            this.lb_contact.AutoSize = true;
            this.lb_contact.Location = new System.Drawing.Point(7, 75);
            this.lb_contact.Name = "lb_contact";
            this.lb_contact.Size = new System.Drawing.Size(68, 13);
            this.lb_contact.TabIndex = 20;
            this.lb_contact.Text = "Liên hệ (10s)";
            // 
            // tb_password
            // 
            this.tb_password.Location = new System.Drawing.Point(83, 46);
            this.tb_password.Name = "tb_password";
            this.tb_password.Size = new System.Drawing.Size(149, 20);
            this.tb_password.TabIndex = 3;
            this.tb_password.Text = "Doanhung123@";
            // 
            // tb_username
            // 
            this.tb_username.Location = new System.Drawing.Point(83, 20);
            this.tb_username.Name = "tb_username";
            this.tb_username.Size = new System.Drawing.Size(149, 20);
            this.tb_username.TabIndex = 2;
            this.tb_username.Text = "0886881999";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Password";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Username";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(533, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Log";
            // 
            // btn_start
            // 
            this.btn_start.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_start.Location = new System.Drawing.Point(1076, 599);
            this.btn_start.Name = "btn_start";
            this.btn_start.Size = new System.Drawing.Size(133, 23);
            this.btn_start.TabIndex = 1;
            this.btn_start.Text = "Start";
            this.btn_start.UseVisualStyleBackColor = true;
            this.btn_start.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1073, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Đã chọn";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(0, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1220, 654);
            this.tabControl1.TabIndex = 6;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1217, 656);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "ProSearch";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox cb_chon_so;
        private System.Windows.Forms.TextBox tb_delay_chon_so;
        private System.Windows.Forms.CheckBox ck_chon_so;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label lb_is_token_alive;
        private System.Windows.Forms.Button btn_logout;
        private System.Windows.Forms.Button btn_login;
        private System.Windows.Forms.CheckBox ck_xac_thuc_tap_doan;
        private System.Windows.Forms.TextBox tb_employee_password;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox tb_employee_account;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.CheckBox ck_bo_0;
        private System.Windows.Forms.CheckBox ck_bo_53;
        private System.Windows.Forms.CheckBox ck_bo_7;
        private System.Windows.Forms.CheckBox ck_bo4;
        private System.Windows.Forms.TextBox tb_found;
        private System.Windows.Forms.TextBox tb_result;
        private System.Windows.Forms.TextBox tb_log;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btn_pause;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox tb_delay;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tb_dang_so;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cb_loai_tb;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tb_lien_he;
        private System.Windows.Forms.Label lb_contact;
        private System.Windows.Forms.TextBox tb_password;
        private System.Windows.Forms.TextBox tb_username;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_start;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cb_province;
        private System.Windows.Forms.ComboBox cb_sale_office;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cb_district;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox tb_bo4;
        private System.Windows.Forms.TextBox tb_bo0;
        private System.Windows.Forms.TextBox tb_bo53;
        private System.Windows.Forms.TextBox tb_bo7;
        private System.Windows.Forms.TextBox tb_contact_count;
        private System.Windows.Forms.Label label10;
    }
}

