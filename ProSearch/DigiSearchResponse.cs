﻿namespace ProSearch
{
    internal class DigiSearchResponse
    {
        public Data data { get; set; }

        public class Data
        {
            public Item[] items { get; set; }
        }

        public class Item
        {
            public string msisdn { get; set; }
        }
    }
}
