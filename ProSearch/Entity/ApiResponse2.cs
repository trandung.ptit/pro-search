﻿using System.Collections.Generic;

namespace ProSearch.Entity
{
    internal class ApiResponse2<T>
    {
        public bool success { get; set; }
        public int statusCode { get; set; }
        public List<T> data { get; set; }
    }
}
