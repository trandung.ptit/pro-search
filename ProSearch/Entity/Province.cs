﻿namespace ProSearch.Entity
{
    internal class Province
    {
        public string id { get; set; }
        public string name { get; set; }
    }
}
