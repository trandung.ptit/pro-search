﻿namespace ProSearch.Entity
{
    internal class District
    {
        public string id { get; set; }
        public string name { get; set; }
        public string quan_id { get; set; }
    }
}
