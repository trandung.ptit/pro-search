﻿namespace ProSearch.Entity
{
    internal class SaleOffice
    {
        public int id { get; set; }
        public int id_pgd { get; set; }
        public string ma { get; set; }
        public string ten { get; set; }
        public string diachi { get; set; }
        public int id_tinh { get; set; }
        public string ma_tinh { get; set; }
        public string ten_tinh { get; set; }
        public int quan_huyen_id { get; set; }
        public string quan_huyen { get; set; }
        public string phuong_xa { get; set; }
        public string ap_khu_pho { get; set; }
        public string so_nha { get; set; }
    }
}
