﻿namespace ProSearch.Entity
{
    internal class ApiResponse<T>
    {
        public bool success { get; set; }
        public int statusCode { get; set; }
        public ApiResponseData<T> Data { get; set; }
    }
}
