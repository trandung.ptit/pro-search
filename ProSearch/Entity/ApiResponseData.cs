﻿
using System.Collections.Generic;

namespace ProSearch.Entity
{
    internal class ApiResponseData<T>
    {
        public int errorCode { get; set; }
        public string errorMessage { get; set; }
        public List<T> items { get; set; }
    }
}
