﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace ProSearch
{
    internal class SoDepUtil
    {
        public static bool Dau088(string somay)
        {
            if (somay.StartsWith("088"))
            {
                return true;
            }
            return false;
        }
        public static bool Dau08(string somay)
        {
            if (somay.StartsWith("08"))
            {
                return true;
            }
            return false;
        }

        public static bool Dau09(string somay)
        {
            if (somay.StartsWith("09"))
            {
                return true;
            }
            return false;
        }

        public static bool LocPhat_x68(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[22]
            {
            "000", "111", "222", "333", "444", "555", "666", "777", "888", "999",
            "668", "686", "688", "868", "886", "866", "234", "345", "456", "567",
            "678", "789"
            };
            string value2 = text5 + text6 + text7;
            string[] source2 = new string[7] { "168", "268", "368", "468", "568", "768", "968" };
            if (source2.Contains(value2) && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool LocPhat_068(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[22]
            {
            "000", "111", "222", "333", "444", "555", "666", "777", "888", "999",
            "668", "686", "688", "868", "886", "866", "234", "345", "456", "567",
            "678", "789"
            };
            string value2 = text5 + text6 + text7;
            string[] source2 = new string[1] { "068" };
            if (source2.Contains(value2) && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool LocPhat3_668(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[22]
            {
            "000", "111", "222", "333", "444", "555", "666", "777", "888", "999",
            "668", "686", "688", "868", "886", "866", "234", "345", "456", "567",
            "678", "789"
            };
            string[] source2 = new string[2] { "6", "8" };
            string text8 = text5 + text6 + text7;
            if (text8 == "668" && !source2.Contains(text4) && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool LocPhat3_688(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[22]
            {
            "000", "111", "222", "333", "444", "555", "666", "777", "888", "999",
            "668", "686", "688", "868", "886", "866", "234", "345", "456", "567",
            "678", "789"
            };
            string[] source2 = new string[2] { "6", "8" };
            string text8 = text5 + text6 + text7;
            if (text8 == "688" && !source2.Contains(text4) && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool LocPhat3_686(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[22]
            {
            "000", "111", "222", "333", "444", "555", "666", "777", "888", "999",
            "668", "686", "688", "868", "886", "866", "234", "345", "456", "567",
            "678", "789"
            };
            string[] source2 = new string[2] { "6", "8" };
            string text8 = text5 + text6 + text7;
            if (text8 == "686" && !source2.Contains(text4) && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool LocPhat3_868(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[22]
            {
            "000", "111", "222", "333", "444", "555", "666", "777", "888", "999",
            "668", "686", "688", "868", "886", "866", "234", "345", "456", "567",
            "678", "789"
            };
            string[] source2 = new string[2] { "6", "8" };
            string text8 = text5 + text6 + text7;
            if (text8 == "868" && !source2.Contains(text4) && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool LocPhat3_886(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[22]
            {
            "000", "111", "222", "333", "444", "555", "666", "777", "888", "999",
            "668", "686", "688", "868", "886", "866", "234", "345", "456", "567",
            "678", "789"
            };
            string[] source2 = new string[2] { "6", "8" };
            string text8 = text5 + text6 + text7;
            if (text8 == "886" && !source2.Contains(text4) && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool LocPhat3_866(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[22]
            {
            "000", "111", "222", "333", "444", "555", "666", "777", "888", "999",
            "668", "686", "688", "868", "886", "866", "234", "345", "456", "567",
            "678", "789"
            };
            string[] source2 = new string[2] { "6", "8" };
            string text8 = text5 + text6 + text7;
            if (text8 == "866" && !source2.Contains(text4) && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool LocPhat3(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[22]
            {
            "000", "111", "222", "333", "444", "555", "666", "777", "888", "999",
            "668", "686", "688", "868", "886", "866", "234", "345", "456", "567",
            "678", "789"
            };
            string[] source2 = new string[2] { "6", "8" };
            string value2 = text5 + text6 + text7;
            string[] source3 = new string[6] { "668", "686", "688", "868", "886", "866" };
            if (source3.Contains(value2) && !source2.Contains(text4) && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool LocPhat3Nut(string somay, string pattern)
        {
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool LocPhat4_6668(string somay)
        {
            string pattern = "([0-9]{5})(6668$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(6668$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(6668$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool LocPhat4_8886(string somay)
        {
            string pattern = "([0-9]{5})(8886$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(8886$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(8886$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool LocPhat4_6686(string somay)
        {
            string pattern = "([0-9]{5})(6686$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(6686$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(6686$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool LocPhat4_6866(string somay)
        {
            string pattern = "([0-9]{5})(6866$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(6866$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(6866$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool LocPhat4_8688(string somay)
        {
            string pattern = "([0-9]{5})(8688$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(8688$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(8688$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool LocPhat4_8868(string somay)
        {
            string pattern = "([0-9]{5})(8868$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(8868$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(8868$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool LocPhat4_8668(string somay)
        {
            string pattern = "([0-9]{5})(8668$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(8668$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(8668$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool LocPhat4_6886(string somay)
        {
            string pattern = "([0-9]{5})(6886$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(6886$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(6886$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool LocPhat4_6688(string somay)
        {
            string pattern = "([0-9]{5})(6688$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(6688$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(6688$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool LocPhat4_8866(string somay)
        {
            string pattern = "([0-9]{5})(8866$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(8866$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(8866$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool LocPhat4_6868(string somay)
        {
            string pattern = "([0-9]{5})(6868$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(6868$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(6868$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool LocPhat4_8686(string somay)
        {
            string pattern = "([0-9]{5})(8686$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(8686$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(8686$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool LocPhat4(string somay)
        {
            string pattern = "([0-9]{5})(6668$|8886$|6686$|6866$|8688$|8868$|8668$|6886$|6688$|8866$|6868$|8686$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(6668$|8886$|6686$|6866$|8688$|8868$|8668$|6886$|6688$|8866$|6868$|8686$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(6668$|8886$|6686$|6866$|8688$|8868$|8668$|6886$|6688$|8866$|6868$|8686$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool LocPhat4Nut(string somay, string pattern)
        {
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool LucQuy(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4 + text5 + text6 + text7;
            string[] source = new string[10] { "000000", "111111", "222222", "333333", "444444", "555555", "666666", "777777", "888888", "999999" };
            if (source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool LucGiua(string somay)
        {
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4 + text5 + text6 + text7;
            string[] source = new string[10] { "000000", "111111", "222222", "333333", "444444", "555555", "666666", "777777", "888888", "999999" };
            if (source.Contains(value) && text8 != text7)
            {
                return true;
            }
            return false;
        }

        public static bool LucGiua_000000(string somay)
        {
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            string text9 = text2 + text3 + text4 + text5 + text6 + text7;
            if (text9 == "000000" && text8 != text7)
            {
                return true;
            }
            return false;
        }

        public static bool LucGiua_111111(string somay)
        {
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            string text9 = text2 + text3 + text4 + text5 + text6 + text7;
            if (text9 == "111111" && text8 != text7)
            {
                return true;
            }
            return false;
        }

        public static bool LucGiua_222222(string somay)
        {
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            string text9 = text2 + text3 + text4 + text5 + text6 + text7;
            if (text9 == "222222" && text8 != text7)
            {
                return true;
            }
            return false;
        }

        public static bool LucGiua_333333(string somay)
        {
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            string text9 = text2 + text3 + text4 + text5 + text6 + text7;
            if (text9 == "333333" && text8 != text7)
            {
                return true;
            }
            return false;
        }

        public static bool LucGiua_444444(string somay)
        {
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            string text9 = text2 + text3 + text4 + text5 + text6 + text7;
            if (text9 == "444444" && text8 != text7)
            {
                return true;
            }
            return false;
        }

        public static bool LucGiua_555555(string somay)
        {
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            string text9 = text2 + text3 + text4 + text5 + text6 + text7;
            if (text9 == "555555" && text8 != text7)
            {
                return true;
            }
            return false;
        }

        public static bool LucGiua_666666(string somay)
        {
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            string text9 = text2 + text3 + text4 + text5 + text6 + text7;
            if (text9 == "666666" && text8 != text7)
            {
                return true;
            }
            return false;
        }

        public static bool LucGiua_777777(string somay)
        {
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            string text9 = text2 + text3 + text4 + text5 + text6 + text7;
            if (text9 == "777777" && text8 != text7)
            {
                return true;
            }
            return false;
        }

        public static bool LucGiua_888888(string somay)
        {
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            string text9 = text2 + text3 + text4 + text5 + text6 + text7;
            if (text9 == "888888" && text8 != text7)
            {
                return true;
            }
            return false;
        }

        public static bool LucGiua_999999(string somay)
        {
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            string text9 = text2 + text3 + text4 + text5 + text6 + text7;
            if (text9 == "999999" && text8 != text7)
            {
                return true;
            }
            return false;
        }

        public static bool LocPhat_6Nut_1(string somay)
        {
            string[] source = new string[6] { "68", "86", "79", "39", "66", "88" };
            string[] source2 = new string[12]
            {
            "6686", "6866", "8688", "8868", "6668", "8886", "6886", "8668", "6868", "8686",
            "6688", "8866"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3) && source2.Contains(text4 + text5 + text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool LocPhat_6Nut_2(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string[] source2 = new string[12]
            {
            "6686", "6866", "8688", "8868", "6668", "8886", "6886", "8668", "6868", "8686",
            "6688", "8866"
            };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && source2.Contains(text5 + text6 + text7 + text8))
            {
                return true;
            }
            return false;
        }

        public static bool LocPhat_6Nut_3(string somay)
        {
            string[] source = new string[10] { "012", "123", "234", "345", "456", "567", "678", "789", "979", "939" };
            string[] source2 = new string[12]
            {
            "6686", "6866", "8688", "8868", "6668", "8886", "6886", "8668", "6868", "8686",
            "6688", "8866"
            };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && source2.Contains(text5 + text6 + text7 + text8))
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_x66(string somay)
        {
            string pattern = "([0-9]{6})(166$|266$|366$|466$|566$|766$|966$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_aba_x66(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string[] source = new string[2] { "866", "666" };
            string text8 = text6 + text7;
            if (text8 == "66" && text2 == text4 && text2 != text3 && !source.Contains(text5 + text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_aab_x66(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string[] source = new string[2] { "866", "666" };
            string text8 = text6 + text7;
            if (text8 == "66" && text2 == text3 && text2 != text4 && !source.Contains(text5 + text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_abb_x66(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string[] source = new string[2] { "866", "666" };
            string text8 = text6 + text7;
            if (text8 == "66" && text3 == text4 && text2 != text3 && !source.Contains(text5 + text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_x88(string somay)
        {
            string pattern = "([0-9]{6})(188$|288$|388$|588$|788$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_aba_x88(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string[] source = new string[2] { "668", "888" };
            string text8 = text6 + text7;
            if (text8 == "88" && text2 == text4 && text2 != text3 && !source.Contains(text5 + text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_aab_x88(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string[] source = new string[2] { "668", "888" };
            string text8 = text6 + text7;
            if (text8 == "88" && text2 == text3 && text2 != text4 && !source.Contains(text5 + text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_abb_x88(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string[] source = new string[2] { "668", "888" };
            string text8 = text6 + text7;
            if (text8 == "88" && text3 == text4 && text2 != text3 && !source.Contains(text5 + text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_x99(string somay)
        {
            string pattern = "([0-9]{6})(199$|299$|399$|599$|699$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_aba_x99(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string[] source = new string[2] { "799", "999" };
            string text8 = text6 + text7;
            if (text8 == "99" && text2 == text4 && text2 != text3 && !source.Contains(text5 + text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_aab_x99(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string[] source = new string[2] { "799", "999" };
            string text8 = text6 + text7;
            if (text8 == "99" && text2 == text3 && text2 != text4 && !source.Contains(text5 + text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_abb_x99(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string[] source = new string[2] { "799", "999" };
            string text8 = text6 + text7;
            if (text8 == "99" && text3 == text4 && text2 != text3 && !source.Contains(text5 + text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_66x(string somay)
        {
            string pattern = "([0-9]{6})(660$|661$|662$|663$|664$|664$|667$|669$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_88x(string somay)
        {
            string pattern = "([0-9]{6})(880$|881$|882$|883$|884$|885$|887$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_99x(string somay)
        {
            string pattern = "([0-9]{6})(990$|991$|992$|993$|994$|995$|996$|997$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_x89(string somay)
        {
            string pattern = "([0-9]{6})(189$|289$|389$|589$|689$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_x69(string somay)
        {
            string pattern = "([0-9]{6})(169$|269$|369$|569$|669$|969$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_9x9(string somay)
        {
            string pattern = "([0-9]{6})(909$|919$|929$|939$|959$|969$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_aba_9x9(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string[] source = new string[2] { "979", "999" };
            string text8 = text5 + text7;
            if (text8 == "99" && text2 == text4 && text2 != text3 && !source.Contains(text5 + text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_8x8(string somay)
        {
            string pattern = "([0-9]{6})(818$|828$|838$|858$|878$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_6x6(string somay)
        {
            string pattern = "([0-9]{6})(616$|626$|636$|656$|696$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_889(string somay)
        {
            string pattern = "([0-9]{6})(889$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_899(string somay)
        {
            string pattern = "([0-9]{6})(899$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_989(string somay)
        {
            string pattern = "([0-9]{6})(989$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_998(string somay)
        {
            string pattern = "([0-9]{6})(998$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_988(string somay)
        {
            string pattern = "([0-9]{6})(988$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_898(string somay)
        {
            string pattern = "([0-9]{6})(898$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_669(string somay)
        {
            string pattern = "([0-9]{6})(669$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_699(string somay)
        {
            string pattern = "([0-9]{6})(699$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_969(string somay)
        {
            string pattern = "([0-9]{6})(969$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_996(string somay)
        {
            string pattern = "([0-9]{6})(996$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_966(string somay)
        {
            string pattern = "([0-9]{6})(966$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_696(string somay)
        {
            string pattern = "([0-9]{6})(696$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_078(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string[] source = new string[1] { "000" };
            string text8 = text5 + text6 + text7;
            if (text8 == "078" && !source.Contains(text2 + text3 + text4))
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_113(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string value = somay.Substring(5, 1);
            string text4 = somay.Substring(6, 1);
            string text5 = somay.Substring(7, 1);
            string text6 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    value = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    value = somay.Substring(6, 1);
                    text4 = somay.Substring(7, 1);
                    text5 = somay.Substring(8, 1);
                    text6 = somay.Substring(9, 1);
                    break;
            }
            string[] source = new string[2] { "1", "3" };
            string text7 = text4 + text5 + text6;
            if (text7 == "113" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_x77(string somay)
        {
            string pattern = "([0-9]{6})(177$|277$|377$|577$|677$|877$|977$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_x55(string somay)
        {
            string pattern = "([0-9]{6})(155$|255$|355$|655$|755$|855$|955$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_x33(string somay)
        {
            string pattern = "([0-9]{6})(133$|233$|533$|633$|733$|833$|933$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_x22(string somay)
        {
            string pattern = "([0-9]{6})(122$|322$|522$|622$|722$|822$|922$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_x11(string somay)
        {
            string pattern = "([0-9]{6})(211$|311$|511$|611$|711$|811$|911$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_x00(string somay)
        {
            string pattern = "([0-9]{6})(100$|200$|300$|500$|600$|800$|900$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_77x(string somay)
        {
            string pattern = "([0-9]{6})(770$|771$|772$|773$|775$|776$|778$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_55x(string somay)
        {
            string pattern = "([0-9]{6})(550$|551$|552$|553$|556$|557$|558$|559$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_33x(string somay)
        {
            string pattern = "([0-9]{6})(330$|331$|332$|335$|336$|337$|338$|339$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_22x(string somay)
        {
            string pattern = "([0-9]{6})(220$|221$|223$|225$|226$|227$|228$|229$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_11x(string somay)
        {
            string pattern = "([0-9]{6})(110$|112$|113$|115$|116$|117$|118$|119$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_00x(string somay)
        {
            string pattern = "([0-9]{6})(001$|002$|003$|005$|006$|007$|008$|009$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_7x7(string somay)
        {
            string pattern = "([0-9]{6})(707$|717$|727$|737$|757$|767$|787$|797$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_5x5(string somay)
        {
            string pattern = "([0-9]{6})(505$|515$|525$|535$|565$|575$|585$|595$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_3x3(string somay)
        {
            string pattern = "([0-9]{6})(303$|313$|323$|353$|363$|373$|383$|393$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_2x2(string somay)
        {
            string pattern = "([0-9]{6})(202$|212$|232$|252$|262$|272$|282$|292$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_1x1(string somay)
        {
            string pattern = "([0-9]{6})(101$|121$|131$|151$|161$|171$|181$|191$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_0x0(string somay)
        {
            string pattern = "([0-9]{6})(010$|020$|030$|050$|060$|070$|080$|090$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_x98(string somay)
        {
            string pattern = "([0-9]{6})(098$|198$|298$|398$|598$|698$|798$|898$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_x97(string somay)
        {
            string pattern = "([0-9]{6})(097$|197$|297$|397$|597$|697$|797$|897$|997$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_x96(string somay)
        {
            string pattern = "([0-9]{6})(096$|196$|296$|396$|596$|696$|796$|896$|996$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_x95(string somay)
        {
            string pattern = "([0-9]{6})(095$|195$|295$|395$|595$|695$|795$|895$|995$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_x94(string somay)
        {
            string pattern = "([0-9]{6})(094$|194$|294$|394$|594$|694$|794$|894$|994$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_x93(string somay)
        {
            string pattern = "([0-9]{6})(093$|193$|293$|393$|593$|693$|793$|893$|993$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_x92(string somay)
        {
            string pattern = "([0-9]{6})(092$|192$|292$|392$|592$|692$|792$|892$|992$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool AAxAAy(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text2 == text3 && text5 == text6 && text2 == text5 && text4 != text7 && text3 != text4 && text6 != text7 && !NamSinh(somay))
            {
                return true;
            }
            return false;
        }

        public static bool ACCBCC(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text3 == text4 && text6 == text7 && text2 != text3 && text3 + text4 == text6 + text7 && Convert.ToInt32(text2) < Convert.ToInt32(text5))
            {
                string[] source = new string[7] { "66", "88", "99", "68", "79", "89", "39" };
                if (source.Contains(text4 + text5))
                {
                    return true;
                }
            }
            return false;
        }

        public static bool AABAAC(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string value = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    value = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    value = somay.Substring(9, 1);
                    break;
            }
            if (text2 == text3 && text5 == text6 && text2 == text5 && text3 != text4 && Convert.ToInt32(text4) < Convert.ToInt32(value) && !TamQuy(somay) && !NamSinh(somay))
            {
                return true;
            }
            return false;
        }

        public static bool ABAACA(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string value = somay.Substring(7, 1);
            string text6 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    value = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    value = somay.Substring(8, 1);
                    text6 = somay.Substring(9, 1);
                    break;
            }
            if (text2 == text4 && text5 == text6 && text2 == text5 && text2 != text3 && Convert.ToInt32(text3) < Convert.ToInt32(value))
            {
                return true;
            }
            return false;
        }

        public static bool abc_acb(string somay)
        {
            string[] source = new string[11]
            {
            "979", "779", "898", "989", "668", "688", "686", "868", "886", "866",
            "889"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text2 == text5 && text3 == text7 && text4 == text6 && text3 != text4 && !source.Contains(text5 + text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool abc_bac(string somay)
        {
            string[] source = new string[11]
            {
            "979", "779", "898", "989", "668", "688", "686", "868", "886", "866",
            "889"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text2 == text6 && text3 == text5 && text4 == text7 && text2 != text3 && text2 != text4 && text3 != text4 && !source.Contains(text5 + text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool abc_bca(string somay)
        {
            string[] source = new string[11]
            {
            "979", "779", "898", "989", "668", "688", "686", "868", "886", "866",
            "889"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text2 == text7 && text3 == text5 && text4 == text6 && text2 != text3 && text2 != text4 && !source.Contains(text5 + text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool abc_cab(string somay)
        {
            string[] source = new string[11]
            {
            "979", "779", "898", "989", "668", "688", "686", "868", "886", "866",
            "889"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text2 == text6 && text3 == text7 && text4 == text5 && text2 != text3 && text2 != text4 && text3 != text4 && !source.Contains(text5 + text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool abc_cba(string somay)
        {
            string[] source = new string[11]
            {
            "979", "779", "898", "989", "668", "688", "686", "868", "886", "866",
            "889"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text2 == text7 && text3 == text6 && text4 == text5 && text2 != text4 && !source.Contains(text5 + text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool abc_abc(string somay)
        {
            string[] source = new string[11]
            {
            "979", "779", "898", "989", "668", "688", "686", "868", "886", "866",
            "889"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text2 == text5 && text3 == text6 && text4 == text7 && !source.Contains(text5 + text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool ab_ab_ab(string somay)
        {
            string[] source = new string[11]
            {
            "979", "779", "898", "989", "668", "688", "686", "868", "886", "866",
            "889"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }

            if (text2 + text3 == text4 + text5 && text4 + text5 == text6 + text7 && !source.Contains(text5 + text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool abc_cba(string somay, string[] duoiso_filer)
        {
            string[] source = new string[11]
            {
            "979", "779", "898", "989", "668", "688", "686", "868", "886", "866",
            "889"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text2 == text7 && text3 == text6 && text4 == text5 && duoiso_filer.Contains(text2 + text3 + text4 + text5 + text6 + text7) && !source.Contains(text5 + text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool aba_cdc(string somay)
        {
            string[] source = new string[11]
            {
            "979", "779", "898", "989", "668", "688", "686", "868", "886", "866",
            "889"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text2 == text4 && text5 == text7 && text2 != text3 && text2 != text5 && text5 != text6 && !source.Contains(text5 + text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool aba_cdc_cMore5(string somay)
        {
            string[] source = new string[11]
            {
            "979", "779", "898", "989", "668", "688", "686", "868", "886", "866",
            "889"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text2 == text4 && text5 == text7 && text2 != text5 && Convert.ToInt32(text5) < Convert.ToInt32(text6) && Convert.ToInt32(text7) >= 5 && !source.Contains(text5 + text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool abb_cdc_cLess5(string somay)
        {
            string[] source = new string[11]
            {
            "979", "779", "898", "989", "668", "688", "686", "868", "886", "866",
            "889"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text3 == text4 && text5 == text7 && text2 != text5 && Convert.ToInt32(text5) < Convert.ToInt32(text6) && Convert.ToInt32(text7) < 5 && !source.Contains(text5 + text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool abb_ccd_cLess5(string somay)
        {
            string[] source = new string[11]
            {
            "979", "779", "898", "989", "668", "688", "686", "868", "886", "866",
            "889"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text3 == text4 && text5 == text6 && text2 != text5 && Convert.ToInt32(text6) < Convert.ToInt32(text7) && Convert.ToInt32(text5) < 5 && !source.Contains(text5 + text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool abb_cdd_cLess5(string somay)
        {
            string[] source = new string[11]
            {
            "979", "779", "898", "989", "668", "688", "686", "868", "886", "866",
            "889"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text3 == text4 && text6 == text7 && text4 != text6 && text2 != text5 && Convert.ToInt32(text5) < Convert.ToInt32(text6) && Convert.ToInt32(text5) < 5 && !source.Contains(text5 + text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool aba_cdd_cLess5(string somay)
        {
            string[] source = new string[11]
            {
            "979", "779", "898", "989", "668", "688", "686", "868", "886", "866",
            "889"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text2 == text4 && text6 == text7 && text2 != text5 && Convert.ToInt32(text5) < Convert.ToInt32(text6) && Convert.ToInt32(text5) < 5 && !source.Contains(text5 + text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool aba_ccd_cLess5(string somay)
        {
            string[] source = new string[11]
            {
            "979", "779", "898", "989", "668", "688", "686", "868", "886", "866",
            "889"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text2 == text3 && text5 == text6 && text2 != text5 && Convert.ToInt32(text6) < Convert.ToInt32(text7) && Convert.ToInt32(text5) < 5 && !source.Contains(text5 + text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool aba_cdc_cLess5(string somay)
        {
            string[] source = new string[11]
            {
            "979", "779", "898", "989", "668", "688", "686", "868", "886", "866",
            "889"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text2 == text3 && text5 == text7 && text2 != text5 && Convert.ToInt32(text5) < Convert.ToInt32(text6) && Convert.ToInt32(text7) < 5 && !source.Contains(text5 + text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool aba_dcc(string somay)
        {
            string[] source = new string[11]
            {
            "979", "779", "898", "989", "668", "688", "686", "868", "886", "866",
            "889"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text2 == text4 && text6 == text7 && text2 != text3 && text3 != text4 && text4 != text5 && text5 != text6 && !source.Contains(text5 + text6 + text7) && !PhongThuy_x66(somay) && !PhongThuy_x88(somay) && !PhongThuy_x99(somay))
            {
                return true;
            }
            return false;
        }

        public static bool aba_ccd(string somay)
        {
            string[] source = new string[11]
            {
            "979", "779", "898", "989", "668", "688", "686", "868", "886", "866",
            "889"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text2 == text4 && text5 == text6 && text6 != text7 && text2 != text7 && text4 != text5 && text2 != text3 && !source.Contains(text5 + text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool caa_dbb(string somay)
        {
            string[] source = new string[11]
            {
            "979", "779", "898", "989", "668", "688", "686", "868", "886", "866",
            "889"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text3 == text4 && text6 == text7 && text2 != text3 && text3 != text6 && text4 != text5 && text5 != text7 && !source.Contains(text5 + text6 + text7) && !PhongThuy_x66(somay) && !PhongThuy_x88(somay) && !PhongThuy_x99(somay))
            {
                return true;
            }
            return false;
        }

        public static bool caa_bdb(string somay)
        {
            string[] source = new string[11]
            {
            "979", "779", "898", "989", "668", "688", "686", "868", "886", "866",
            "889"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text3 == text4 && text5 == text7 && text2 != text3 && text4 != text5 && text4 != text6 && text5 != text6 && !source.Contains(text5 + text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool caa_bbd(string somay)
        {
            string[] source = new string[11]
            {
            "979", "779", "898", "989", "668", "688", "686", "868", "886", "866",
            "889"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text3 == text4 && text5 == text6 && text4 != text5 && text4 != text7 && text2 != text3 && text6 != text7 && !source.Contains(text5 + text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool aac_dbb(string somay)
        {
            string[] source = new string[11]
            {
            "979", "779", "898", "989", "668", "688", "686", "868", "886", "866",
            "889"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text2 == text3 && text6 == text7 && text3 != text4 && text4 != text5 && text5 != text6 && !source.Contains(text5 + text6 + text7) && !PhongThuy_x66(somay) && !PhongThuy_x88(somay) && !PhongThuy_x99(somay))
            {
                return true;
            }
            return false;
        }

        public static bool aac_bdb(string somay)
        {
            string[] source = new string[11]
            {
            "979", "779", "898", "989", "668", "688", "686", "868", "886", "866",
            "889"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text2 == text3 && text5 == text7 && text3 != text4 && text5 != text6 && !ABAB_All(somay) && !BABA_All(somay) && !source.Contains(text5 + text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool aac_bbd(string somay)
        {
            string[] source = new string[11]
            {
            "979", "779", "898", "989", "668", "688", "686", "868", "886", "866",
            "889"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text2 == text3 && text5 == text6 && text4 != text7 && text2 != text3 && !source.Contains(text5 + text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool aaa_ganhkep(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            if (source.Contains(value) && (text5 == text6 || text6 == text7 || text5 == text7))
            {
                return true;
            }
            return false;
        }

        public static bool aba_cdc_Tien(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string value = somay.Substring(4, 1);
            string text3 = somay.Substring(5, 1);
            string text4 = somay.Substring(6, 1);
            string text5 = somay.Substring(7, 1);
            string text6 = somay.Substring(8, 1);
            if (text2 == text3 && text4 == text6 && Convert.ToInt32(value) < Convert.ToInt32(text5) && text4 != text5)
            {
                return true;
            }
            return false;
        }

        public static bool aba_dcc_Tien(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string value = somay.Substring(4, 1);
            string text3 = somay.Substring(5, 1);
            string value2 = somay.Substring(6, 1);
            string text4 = somay.Substring(7, 1);
            string text5 = somay.Substring(8, 1);
            if (text2 == text3 && text4 == text5 && Convert.ToInt32(text2) < Convert.ToInt32(value2) && Convert.ToInt32(value) < Convert.ToInt32(text4))
            {
                return true;
            }
            return false;
        }

        public static bool aba_ccd_Tien(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string value = somay.Substring(4, 1);
            string text3 = somay.Substring(5, 1);
            string text4 = somay.Substring(6, 1);
            string text5 = somay.Substring(7, 1);
            string value2 = somay.Substring(8, 1);
            if (text2 == text3 && text4 == text5 && Convert.ToInt32(text2) < Convert.ToInt32(text4) && Convert.ToInt32(value) < Convert.ToInt32(value2))
            {
                return true;
            }
            return false;
        }

        public static bool caa_dbb_Tien(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string value = somay.Substring(6, 1);
            string text5 = somay.Substring(7, 1);
            string text6 = somay.Substring(8, 1);
            if (text3 == text4 && text5 == text6 && text2 != text3 && Convert.ToInt32(text2) < Convert.ToInt32(value) && Convert.ToInt32(text3) < Convert.ToInt32(text5))
            {
                return true;
            }
            return false;
        }

        public static bool caa_bdb_Tien(string somay)
        {
            string text = somay.Substring(0, 3);
            string value = somay.Substring(3, 1);
            string text2 = somay.Substring(4, 1);
            string text3 = somay.Substring(5, 1);
            string text4 = somay.Substring(6, 1);
            string value2 = somay.Substring(7, 1);
            string text5 = somay.Substring(8, 1);
            if (text2 == text3 && text4 == text5 && Convert.ToInt32(value) < Convert.ToInt32(text4) && Convert.ToInt32(text2) < Convert.ToInt32(value2))
            {
                return true;
            }
            return false;
        }

        public static bool caa_bbd_Tien(string somay)
        {
            string text = somay.Substring(0, 3);
            string value = somay.Substring(3, 1);
            string text2 = somay.Substring(4, 1);
            string text3 = somay.Substring(5, 1);
            string text4 = somay.Substring(6, 1);
            string text5 = somay.Substring(7, 1);
            string value2 = somay.Substring(8, 1);
            if (text2 == text3 && text4 == text5 && Convert.ToInt32(value) < Convert.ToInt32(text4) && Convert.ToInt32(text2) < Convert.ToInt32(value2))
            {
                return true;
            }
            return false;
        }

        public static bool aac_dbb_Tien(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string value = somay.Substring(5, 1);
            string value2 = somay.Substring(6, 1);
            string text4 = somay.Substring(7, 1);
            string text5 = somay.Substring(8, 1);
            if (text2 == text3 && text4 == text5 && Convert.ToInt32(text2) < Convert.ToInt32(value2) && Convert.ToInt32(value) < Convert.ToInt32(text4))
            {
                return true;
            }
            return false;
        }

        public static bool aac_bdb_Tien(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string value = somay.Substring(5, 1);
            string text4 = somay.Substring(6, 1);
            string value2 = somay.Substring(7, 1);
            string text5 = somay.Substring(8, 1);
            if (text2 == text3 && text4 == text5 && Convert.ToInt32(text2) < Convert.ToInt32(text4) && Convert.ToInt32(value) < Convert.ToInt32(value2))
            {
                return true;
            }
            return false;
        }

        public static bool aac_bbd_Tien(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string value = somay.Substring(5, 1);
            string text4 = somay.Substring(6, 1);
            string text5 = somay.Substring(7, 1);
            string value2 = somay.Substring(8, 1);
            if (text2 == text3 && text4 == text5 && Convert.ToInt32(text2) < Convert.ToInt32(text4) && Convert.ToInt32(value) < Convert.ToInt32(value2))
            {
                return true;
            }
            return false;
        }

        public static bool xy_ab_ad(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string value = somay.Substring(6, 1);
            string text5 = somay.Substring(7, 1);
            string value2 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    value = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    value2 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    value = somay.Substring(7, 1);
                    text5 = somay.Substring(8, 1);
                    value2 = somay.Substring(9, 1);
                    break;
            }
            if (text4 == text5 && Convert.ToInt32(value2) == Convert.ToInt32(value) + 1)
            {
                return true;
            }
            return false;
        }

        public static bool xy_ad_bd(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string value = somay.Substring(5, 1);
            string text4 = somay.Substring(6, 1);
            string text5 = somay.Substring(7, 1);
            string text6 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    value = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    value = somay.Substring(6, 1);
                    text4 = somay.Substring(7, 1);
                    text5 = somay.Substring(8, 1);
                    text6 = somay.Substring(9, 1);
                    break;
            }
            if (Convert.ToInt32(text5) <= 9 && text4 == text6 && Convert.ToInt32(text5) == Convert.ToInt32(value) + 1 && text4 != text5)
            {
                return true;
            }
            return false;
        }

        public static bool ab_ab_ac(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string value = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    value = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    value = somay.Substring(9, 1);
                    break;
            }
            if (Convert.ToInt32(value) <= 9)
            {
                string text7 = text2 + text3;
                string text8 = text4 + text5;
                if (text7 == text8 && text2 == text6 && Convert.ToInt32(text2) < Convert.ToInt32(text3) && Convert.ToInt32(text5) < Convert.ToInt32(value))
                {
                    return true;
                }
            }
            return false;
        }

        public static bool SoDep4Nut(string somay)
        {
            string pattern = "([0-9]{5})(8889$|8989$|9889$|8998$|9898$|9988$|8899$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool x39_y79(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text3 + text4 == "39" && text6 + text7 == "79" && text2 != "0" && text5 != "0")
            {
                return true;
            }
            return false;
        }

        public static bool x68_y79(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text3 + text4 == "68" && text6 + text7 == "79" && text2 != "0" && text5 != "0")
            {
                return true;
            }
            return false;
        }

        public static bool x86_y79(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text3 + text4 == "86" && text6 + text7 == "79" && text2 != "0" && text5 != "0")
            {
                return true;
            }
            return false;
        }

        public static bool x68_y86(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text3 + text4 == "68" && text6 + text7 == "86" && text2 != "0" && text5 != "0")
            {
                return true;
            }
            return false;
        }

        public static bool x86_y68(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text3 + text4 == "86" && text6 + text7 == "68" && text2 != "0" && text5 != "0")
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_6868_xy(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5;
            if (text8 == "6868")
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_8686_xy(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5;
            if (text8 == "8686")
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_6688_xy(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5;
            if (text8 == "6688")
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_8866_xy(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5;
            if (text8 == "8866")
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_8668_xy(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5;
            if (text8 == "8668")
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_6886_xy(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5;
            if (text8 == "6886")
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_6668_xy(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5;
            if (text8 == "6668")
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_8886_xy(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5;
            if (text8 == "8886")
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_7979_xy(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5;
            if (text8 == "7979")
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_3939_xy(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5;
            if (text8 == "3939")
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_7779_xy(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5;
            if (text8 == "7779")
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_3339_xy(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5;
            if (text8 == "3339")
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_8868_xy(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5;
            if (text8 == "8868")
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_5252_xy(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5;
            if (text8 == "5252")
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_68_xy_68(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3;
            string text9 = text6 + text7;
            if (text8 == "68" && text9 == "68")
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_79_xy_79(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3;
            string text9 = text6 + text7;
            if (text8 == "79" && text9 == "79")
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_69_xy_69(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3;
            string text9 = text6 + text7;
            if (text8 == "69" && text9 == "69")
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_39_xy_39(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3;
            string text9 = text6 + text7;
            if (text8 == "39" && text9 == "39")
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_52_xy_52(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3;
            string text9 = text6 + text7;
            if (text8 == "52" && text9 == "52")
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_a52_52a(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string[] source = new string[2] { "2", "5" };
            string text8 = text3 + text4 + text5 + text6;
            if (text8 == "5252" && text2 == text7 && !source.Contains(text2))
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_a68_68a(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string[] source = new string[3] { "6", "8", "9" };
            string text8 = text3 + text4 + text5 + text6;
            if (text8 == "6868" && text2 == text7 && !source.Contains(text2))
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_a86_86a(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string[] source = new string[3] { "6", "8", "9" };
            string text8 = text3 + text4 + text5 + text6;
            if (text8 == "8686" && text2 == text7 && !source.Contains(text2))
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_a98_98a(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string[] source = new string[3] { "6", "8", "9" };
            string text8 = text3 + text4 + text5 + text6;
            if (text8 == "9898" && text2 == text7 && !source.Contains(text2))
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy_a79_79a(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string[] source = new string[3] { "7", "8", "9" };
            string text8 = text3 + text4 + text5 + text6;
            if (text8 == "7979" && text2 == text7 && !source.Contains(text2))
            {
                return true;
            }
            return false;
        }

        public static bool PhongThuy3Nut(string somay)
        {
            string pattern = "([0-9]{6})(189$|289$|389$|589$|689$|989$|779$|799$|889$|899$|\r\n                                168$|268$|368$|468$|568$|668$|768$|868$|968$|\r\n                                179$|279$|379$|479$|579$|679$|779$|879$|\r\n                                099$|199$|299$|399$|599$|699$|188$|288$|388$|588$|688$|788$|988$|998$|\r\n                                808$|818$|828$|838$|858$|878$|898$|\r\n                                880$|881$|883$|884$|885$|886$|887$|\r\n                                909$|919$|929$|939$|959$|969$|979$|\r\n                                990$|991$|992$|993$|994$|995$|996$|997$|998$|\r\n                                177$|277$|377$|577$|677$|877$|977$|717$|727$|737$|757$|767$|787$|797$|\r\n                                166$|266$|366$|466$|566$|766$|866$|966$|626$|636$|656$|696$|\r\n                                660$|661$|662$|663$|664$|665$|667$|667$|669$|\r\n                                055$|155$|255$|355$|455$|655$|755$|855$|955$|505$|515$|525$|535$|565$|575$|585$|595$|\r\n                                550$|551$|552$|556$|557$|558$|559$|\r\n                                033$|133$|233$|633$|833$|323$|343$|363$|373$|383$|393$|  \r\n                                330$|331$|332$|334$|335$|336$|337$|338$|339$|\r\n                                223$|225$|226$|228$|229$|232$|252$|262$|272$|282$|292$|\r\n                                186$|286$|386$|486$|586$|686$|786$|886$|986$|\r\n                                161$|181$|191$|                            \r\n                                012$|123$|234$|345$|456$|567$)";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool ab_ab_xy(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text2 == text4 && text3 == text5 && text2 != text3 && text4 != text6 && text5 != text6 && Convert.ToInt32(text2) < Convert.ToInt32(text3) && !NamSinh_ddmmyy(somay) && !xy_aaab(somay) && !PhongThuy_6868_xy(somay) && !PhongThuy_8686_xy(somay) && !PhongThuy_6688_xy(somay) && !PhongThuy_8866_xy(somay) && !PhongThuy_8668_xy(somay) && !PhongThuy_6886_xy(somay) && !PhongThuy_6668_xy(somay) && !PhongThuy_8886_xy(somay) && !PhongThuy_7979_xy(somay) && !PhongThuy_3939_xy(somay) && !PhongThuy_7779_xy(somay) && !PhongThuy_3339_xy(somay) && !PhongThuy_8868_xy(somay) && !PhongThuy_5252_xy(somay) && !PhatLoc_x86(somay))
            {
                return true;
            }
            return false;
        }

        public static bool ab_xy_ab(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text2 == text6 && text3 == text7 && text2 != text3 && text2 != text4 && text3 != text4 && Convert.ToInt32(text2) < Convert.ToInt32(text3) && !NamSinh_ddmmyy(somay) && !LocPhat3(somay) && !ThanTai3(somay) && !xy_aaab(somay) && !PhongThuy_68_xy_68(somay) && !PhongThuy_79_xy_79(somay) && !PhongThuy_69_xy_69(somay) && !PhongThuy_39_xy_39(somay) && !PhongThuy_52_xy_52(somay))
            {
                return true;
            }
            return false;
        }

        public static bool aa_bb_xy(string somay)
        {
            string[] source = new string[2] { "68", "79" };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text2 == text3 && text4 == text5 && text2 != text4 && text6 != text7 && Convert.ToInt32(text2) < Convert.ToInt32(text4) && !NamSinh_ddmmyy(somay) && !xy_aaab(somay) && !source.Contains(text6 + text7) && !PhongThuy_6868_xy(somay) && !PhongThuy_8686_xy(somay) && !PhongThuy_6688_xy(somay) && !PhongThuy_8866_xy(somay) && !PhongThuy_8668_xy(somay) && !PhongThuy_6886_xy(somay) && !PhongThuy_6668_xy(somay) && !PhongThuy_8886_xy(somay) && !PhongThuy_7979_xy(somay) && !PhongThuy_3939_xy(somay) && !PhongThuy_7779_xy(somay) && !PhongThuy_3339_xy(somay) && !PhongThuy_8868_xy(somay) && !PhongThuy_5252_xy(somay))
            {
                return true;
            }
            return false;
        }

        public static bool xy_aaab(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text3 != text4 && text4 == text5 && text5 == text6 && text7 != text6)
            {
                return true;
            }
            return false;
        }

        public static bool x_aaaab(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text2 != text3 && text3 == text4 && text4 == text5 && text5 == text6 && text7 != text6)
            {
                return true;
            }
            return false;
        }

        public static bool ab_ac_ad(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text2 == text4 && text4 == text6 && text2 != text3 && text3 != text5 && text5 != text7 && !Tien1(somay))
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text4 + text5 + text6 + text7;
            string[] source = new string[50]
            {
            "1970", "1971", "1972", "1973", "1974", "1975", "1976", "1977", "1978", "1979",
            "1980", "1981", "1982", "1983", "1984", "1985", "1986", "1987", "1988", "1989",
            "1990", "1991", "1992", "1993", "1994", "1995", "1996", "1997", "1998", "1999",
            "2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009",
            "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019"
            };
            if (source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_1979(string somay)
        {
            string pattern = "([0-9]{5})(1979$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(1979$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(1979$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_1979(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1979")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_1979(string somay)
        {
            string[] source = new string[7] { "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1979")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_1980(string somay)
        {
            string pattern = "([0-9]{5})(1980$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(1980$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(1980$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_1980(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1980")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_1980(string somay)
        {
            string[] source = new string[7] { "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1980")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_1981(string somay)
        {
            string pattern = "([0-9]{5})(1981$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(1981$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(1981$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_1981(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1981")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_1981(string somay)
        {
            string[] source = new string[7] { "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1981")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_1982(string somay)
        {
            string pattern = "([0-9]{5})(1982$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(1982$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(1982$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_1982(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1982")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_1982(string somay)
        {
            string[] source = new string[7] { "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1982")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_1983(string somay)
        {
            string pattern = "([0-9]{5})(1983$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(1983$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(1983$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_1983(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1983")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_1983(string somay)
        {
            string[] source = new string[7] { "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1983")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_1984(string somay)
        {
            string pattern = "([0-9]{5})(1984$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(1984$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(1984$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_1984(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1984")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_1984(string somay)
        {
            string[] source = new string[7] { "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1984")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_1985(string somay)
        {
            string pattern = "([0-9]{5})(1985$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(1985$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(1985$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_1985(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1985")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_1985(string somay)
        {
            string[] source = new string[7] { "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1985")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_1986(string somay)
        {
            string pattern = "([0-9]{5})(1986$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(1986$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(1986$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_1986(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1986")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_1986(string somay)
        {
            string[] source = new string[7] { "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1986")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_1987(string somay)
        {
            string pattern = "([0-9]{5})(1987$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(1987$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(1987$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_1987(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1987")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_1987(string somay)
        {
            string[] source = new string[7] { "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1987")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_1988(string somay)
        {
            string pattern = "([0-9]{5})(1988$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(1988$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(1988$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_1988(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1988")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_1988(string somay)
        {
            string[] source = new string[7] { "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1988")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_1989(string somay)
        {
            string pattern = "([0-9]{5})(1989$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(1989$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(1989$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_1989(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1989")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_1989(string somay)
        {
            string[] source = new string[7] { "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1989")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_1990(string somay)
        {
            string pattern = "([0-9]{5})(1990$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(1990$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(1990$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_1990(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1990")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_1990(string somay)
        {
            string[] source = new string[7] { "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1990")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_1991(string somay)
        {
            string pattern = "([0-9]{5})(1991$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(1991$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(1991$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_1991(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1991")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_1991(string somay)
        {
            string[] source = new string[7] { "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1991")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_1992(string somay)
        {
            string pattern = "([0-9]{5})(1992$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(1992$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(1992$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_1992(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1992")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_1992(string somay)
        {
            string[] source = new string[7] { "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1992")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_1993(string somay)
        {
            string pattern = "([0-9]{5})(1993$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(1993$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(1993$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_1993(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1993")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_1993(string somay)
        {
            string[] source = new string[7] { "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1993")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_1994(string somay)
        {
            string pattern = "([0-9]{5})(1994$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(1994$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(1994$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_1994(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1994")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_1994(string somay)
        {
            string[] source = new string[7] { "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1994")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_1995(string somay)
        {
            string pattern = "([0-9]{5})(1995$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(1995$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(1995$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_1995(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1995")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_1995(string somay)
        {
            string[] source = new string[7] { "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1995")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_1996(string somay)
        {
            string pattern = "([0-9]{5})(1996$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(1996$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(1996$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_1996(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1996")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_1996(string somay)
        {
            string[] source = new string[7] { "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1996")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_1997(string somay)
        {
            string pattern = "([0-9]{5})(1997$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(1997$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(1997$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_1997(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1997")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_1997(string somay)
        {
            string[] source = new string[7] { "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1997")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_1998(string somay)
        {
            string pattern = "([0-9]{5})(1998$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(1998$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(1998$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_1998(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1998")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_1998(string somay)
        {
            string[] source = new string[7] { "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1998")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_1999(string somay)
        {
            string pattern = "([0-9]{5})(1999$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(1999$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(1999$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_1999(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1999")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_1999(string somay)
        {
            string[] source = new string[7] { "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "1999")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_2000(string somay)
        {
            string pattern = "([0-9]{5})(2000$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(2000$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(2000$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep2_2000(string somay)
        {
            string[] source = new string[14]
            {
            "68", "86", "79", "39", "00", "11", "22", "33", "44", "55",
            "66", "77", "88", "99"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3) && text4 + text5 + text6 + text7 == "2000")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_2000(string somay)
        {
            string[] source = new string[8] { "012", "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2000")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_2000(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2000")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_2001(string somay)
        {
            string pattern = "([0-9]{5})(2001$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(2001$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(2001$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep2_2001(string somay)
        {
            string[] source = new string[14]
            {
            "68", "86", "79", "39", "00", "11", "22", "33", "44", "55",
            "66", "77", "88", "99"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3) && text4 + text5 + text6 + text7 == "2000")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_2001(string somay)
        {
            string[] source = new string[8] { "012", "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2001")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_2001(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2001")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_2002(string somay)
        {
            string pattern = "([0-9]{5})(2002$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(2002$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(2002$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep2_2002(string somay)
        {
            string[] source = new string[14]
            {
            "68", "86", "79", "39", "00", "11", "22", "33", "44", "55",
            "66", "77", "88", "99"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3) && text4 + text5 + text6 + text7 == "2002")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_2002(string somay)
        {
            string[] source = new string[8] { "012", "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2002")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_2002(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2002")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_2003(string somay)
        {
            string pattern = "([0-9]{5})(2003$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(2003$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(2003$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep2_2003(string somay)
        {
            string[] source = new string[14]
            {
            "68", "86", "79", "39", "00", "11", "22", "33", "44", "55",
            "66", "77", "88", "99"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3) && text4 + text5 + text6 + text7 == "2003")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_2003(string somay)
        {
            string[] source = new string[8] { "012", "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2003")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_2003(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2003")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_2004(string somay)
        {
            string pattern = "([0-9]{5})(2004$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(2004$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(2004$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep2_2004(string somay)
        {
            string[] source = new string[14]
            {
            "68", "86", "79", "39", "00", "11", "22", "33", "44", "55",
            "66", "77", "88", "99"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3) && text4 + text5 + text6 + text7 == "2004")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_2004(string somay)
        {
            string[] source = new string[8] { "012", "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2004")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_2004(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2004")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_2005(string somay)
        {
            string pattern = "([0-9]{5})(2005$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(2005$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(2005$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep2_2005(string somay)
        {
            string[] source = new string[14]
            {
            "68", "86", "79", "39", "00", "11", "22", "33", "44", "55",
            "66", "77", "88", "99"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3) && text4 + text5 + text6 + text7 == "2005")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_2005(string somay)
        {
            string[] source = new string[8] { "012", "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2005")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_2005(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2005")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_2006(string somay)
        {
            string pattern = "([0-9]{5})(2006$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(2006$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(2006$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep2_2006(string somay)
        {
            string[] source = new string[14]
            {
            "68", "86", "79", "39", "00", "11", "22", "33", "44", "55",
            "66", "77", "88", "99"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3) && text4 + text5 + text6 + text7 == "2006")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_2006(string somay)
        {
            string[] source = new string[8] { "012", "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2006")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_2006(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2006")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_2007(string somay)
        {
            string pattern = "([0-9]{5})(2007$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(2007$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(2007$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep2_2007(string somay)
        {
            string[] source = new string[14]
            {
            "68", "86", "79", "39", "00", "11", "22", "33", "44", "55",
            "66", "77", "88", "99"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3) && text4 + text5 + text6 + text7 == "2007")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_2007(string somay)
        {
            string[] source = new string[8] { "012", "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2007")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_2007(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2007")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_2008(string somay)
        {
            string pattern = "([0-9]{5})(2008$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(2008$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(2008$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep2_2008(string somay)
        {
            string[] source = new string[14]
            {
            "68", "86", "79", "39", "00", "11", "22", "33", "44", "55",
            "66", "77", "88", "99"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3) && text4 + text5 + text6 + text7 == "2008")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_2008(string somay)
        {
            string[] source = new string[8] { "012", "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2008")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_2008(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2008")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_2009(string somay)
        {
            string pattern = "([0-9]{5})(2009$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(2009$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(2009$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep2_2009(string somay)
        {
            string[] source = new string[14]
            {
            "68", "86", "79", "39", "00", "11", "22", "33", "44", "55",
            "66", "77", "88", "99"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3) && text4 + text5 + text6 + text7 == "2009")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_2009(string somay)
        {
            string[] source = new string[8] { "012", "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2009")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_2009(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2009")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_2010(string somay)
        {
            string pattern = "([0-9]{5})(2010$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(2010$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(2010$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep2_2010(string somay)
        {
            string[] source = new string[14]
            {
            "68", "86", "79", "39", "00", "11", "22", "33", "44", "55",
            "66", "77", "88", "99"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3) && text4 + text5 + text6 + text7 == "2010")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_2010(string somay)
        {
            string[] source = new string[8] { "012", "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2010")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_2010(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2010")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_2011(string somay)
        {
            string pattern = "([0-9]{5})(2011$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(2011$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(2011$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep2_2011(string somay)
        {
            string[] source = new string[14]
            {
            "68", "86", "79", "39", "00", "11", "22", "33", "44", "55",
            "66", "77", "88", "99"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3) && text4 + text5 + text6 + text7 == "2011")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_2011(string somay)
        {
            string[] source = new string[8] { "012", "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2011")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_2011(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2011")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_2012(string somay)
        {
            string pattern = "([0-9]{5})(2012$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(2012$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(2012$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep2_2012(string somay)
        {
            string[] source = new string[14]
            {
            "68", "86", "79", "39", "00", "11", "22", "33", "44", "55",
            "66", "77", "88", "99"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3) && text4 + text5 + text6 + text7 == "2012")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_2012(string somay)
        {
            string[] source = new string[8] { "012", "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2012")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_2012(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2012")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_2013(string somay)
        {
            string pattern = "([0-9]{5})(2013$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(2013$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(2013$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep2_2013(string somay)
        {
            string[] source = new string[14]
            {
            "68", "86", "79", "39", "00", "11", "22", "33", "44", "55",
            "66", "77", "88", "99"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3) && text4 + text5 + text6 + text7 == "2013")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_2013(string somay)
        {
            string[] source = new string[8] { "012", "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2013")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_2013(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2013")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_2014(string somay)
        {
            string pattern = "([0-9]{5})(2014$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(2014$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(2014$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep2_2014(string somay)
        {
            string[] source = new string[14]
            {
            "68", "86", "79", "39", "00", "11", "22", "33", "44", "55",
            "66", "77", "88", "99"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3) && text4 + text5 + text6 + text7 == "2014")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_2014(string somay)
        {
            string[] source = new string[8] { "012", "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2014")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_2014(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2014")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_2015(string somay)
        {
            string pattern = "([0-9]{5})(2015$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(2015$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(2015$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep2_2015(string somay)
        {
            string[] source = new string[14]
            {
            "68", "86", "79", "39", "00", "11", "22", "33", "44", "55",
            "66", "77", "88", "99"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3) && text4 + text5 + text6 + text7 == "2015")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_2015(string somay)
        {
            string[] source = new string[8] { "012", "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2015")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_2015(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2015")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_2016(string somay)
        {
            string pattern = "([0-9]{5})(2016$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(2016$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(2016$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep2_2016(string somay)
        {
            string[] source = new string[14]
            {
            "68", "86", "79", "39", "00", "11", "22", "33", "44", "55",
            "66", "77", "88", "99"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3) && text4 + text5 + text6 + text7 == "2016")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_2016(string somay)
        {
            string[] source = new string[8] { "012", "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2016")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_2016(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2016")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_2017(string somay)
        {
            string pattern = "([0-9]{5})(2017$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(2017$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(2017$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep2_2017(string somay)
        {
            string[] source = new string[14]
            {
            "68", "86", "79", "39", "00", "11", "22", "33", "44", "55",
            "66", "77", "88", "99"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3) && text4 + text5 + text6 + text7 == "2017")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_2017(string somay)
        {
            string[] source = new string[8] { "012", "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2017")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_2017(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2017")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_2018(string somay)
        {
            string pattern = "([0-9]{5})(2018$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(2018$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(2018$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep2_2018(string somay)
        {
            string[] source = new string[14]
            {
            "68", "86", "79", "39", "00", "11", "22", "33", "44", "55",
            "66", "77", "88", "99"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3) && text4 + text5 + text6 + text7 == "2018")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_2018(string somay)
        {
            string[] source = new string[8] { "012", "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2018")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_2018(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2018")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_2019(string somay)
        {
            string pattern = "([0-9]{5})(2019$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(2019$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(2019$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep2_2019(string somay)
        {
            string[] source = new string[14]
            {
            "68", "86", "79", "39", "00", "11", "22", "33", "44", "55",
            "66", "77", "88", "99"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3) && text4 + text5 + text6 + text7 == "2019")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_dep3_2019(string somay)
        {
            string[] source = new string[8] { "012", "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2019")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_aaa_2019(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 + text8 == "2019")
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_197x(string somay)
        {
            string pattern = "([0-9]{5})(1979$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(1979$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(1979$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_198x(string somay)
        {
            string pattern = "([0-9]{5})(1980$|1981$|1982$|1983$|1984$|1985$|1986$|1987$|1988$|1989$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(1980$|1981$|1982$|1983$|1984$|1985$|1986$|1987$|1988$|1989$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(1980$|1981$|1982$|1983$|1984$|1985$|1986$|1987$|1988$|1989$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_199x(string somay)
        {
            string pattern = "([0-9]{5})(1990$|1991$|1992$|1993$|1994$|1995$|1996$|1997$|1998$|1999$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(1990$|1991$|1992$|1993$|1994$|1995$|1996$|1997$|1998$|1999$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(1990$|1991$|1992$|1993$|1994$|1995$|1996$|1997$|1998$|1999$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_200x(string somay)
        {
            string pattern = "([0-9]{5})(2000$|2001$|2002$|2003$|2004$|2005$|2006$|2007$|2008$|2009$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(2000$|2001$|2002$|2003$|2004$|2005$|2006$|2007$|2008$|2009$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(2000$|2001$|2002$|2003$|2004$|2005$|2006$|2007$|2008$|2009$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_201x(string somay)
        {
            string pattern = "([0-9]{5})(2010$|2011$|2012$|2013$|2014$|2015$|2016$|2017$|2018$|2019$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(2010$|2011$|2012$|2013$|2014$|2015$|2016$|2017$|2018$|2019$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(2010$|2011$|2012$|2013$|2014$|2015$|2016$|2017$|2018$|2019$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool NamSinh_ddmmyy(string somay)
        {
            string text = somay.Substring(0, 3);
            string value = somay.Substring(3, 2);
            string text2 = somay.Substring(5, 2);
            string value2 = somay.Substring(7, 2);
            if (Convert.ToInt32(value) >= 1 && Convert.ToInt32(value) <= 31 && Convert.ToInt32(text2) >= 1 && Convert.ToInt32(text2) <= 12 && Convert.ToInt32(value2) >= 80 && Convert.ToInt32(value2) <= 99)
            {
                switch (text2)
                {
                    case "02":
                        if (Convert.ToInt32(value) <= 28)
                        {
                            return true;
                        }
                        break;
                    default:
                        if (!(text2 == "12"))
                        {
                            switch (text2)
                            {
                                default:
                                    if (!(text2 == "11"))
                                    {
                                        break;
                                    }
                                    goto case "04";
                                case "04":
                                case "06":
                                case "09":
                                    if (Convert.ToInt32(value) <= 30)
                                    {
                                        return true;
                                    }
                                    break;
                            }
                            break;
                        }
                        goto case "01";
                    case "01":
                    case "03":
                    case "05":
                    case "07":
                    case "08":
                    case "10":
                        if (Convert.ToInt32(value) <= 31)
                        {
                            return true;
                        }
                        break;
                }
            }
            return false;
        }

        public static bool NamSinh_ddmmyy(string somay, int minyy, int maxyy)
        {
            string text = somay.Substring(0, 3);
            string value = somay.Substring(3, 2);
            string text2 = somay.Substring(5, 2);
            string value2 = somay.Substring(7, 2);
            if (Convert.ToInt32(value) >= 1 && Convert.ToInt32(value) <= 31 && Convert.ToInt32(text2) >= 1 && Convert.ToInt32(text2) <= 12 && Convert.ToInt32(value2) >= minyy && Convert.ToInt32(value2) <= maxyy)
            {
                switch (text2)
                {
                    case "02":
                        if (Convert.ToInt32(value) <= 28)
                        {
                            return true;
                        }
                        break;
                    default:
                        if (!(text2 == "12"))
                        {
                            switch (text2)
                            {
                                default:
                                    if (!(text2 == "11"))
                                    {
                                        break;
                                    }
                                    goto case "04";
                                case "04":
                                case "06":
                                case "09":
                                    if (Convert.ToInt32(value) <= 30)
                                    {
                                        return true;
                                    }
                                    break;
                            }
                            break;
                        }
                        goto case "01";
                    case "01":
                    case "03":
                    case "05":
                    case "07":
                    case "08":
                    case "10":
                        if (Convert.ToInt32(value) <= 31)
                        {
                            return true;
                        }
                        break;
                }
            }
            return false;
        }

        public static bool NguQuy(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text3 + text4 + text5 + text6 + text7;
            string[] source = new string[10] { "00000", "11111", "22222", "33333", "44444", "55555", "66666", "77777", "88888", "99999" };
            if (source.Contains(value) && text2 != text3)
            {
                return true;
            }
            return false;
        }

        public static bool NguQuy_00000(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text3 + text4 + text5 + text6 + text7;
            if (text8 == "00000" && text2 != "0")
            {
                return true;
            }
            return false;
        }

        public static bool NguQuy_11111(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text3 + text4 + text5 + text6 + text7;
            if (text8 == "11111" && text2 != "1")
            {
                return true;
            }
            return false;
        }

        public static bool NguQuy_22222(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text3 + text4 + text5 + text6 + text7;
            if (text8 == "22222" && text2 != "2")
            {
                return true;
            }
            return false;
        }

        public static bool NguQuy_33333(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text3 + text4 + text5 + text6 + text7;
            if (text8 == "33333" && text2 != "3")
            {
                return true;
            }
            return false;
        }

        public static bool NguQuy_44444(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text3 + text4 + text5 + text6 + text7;
            if (text8 == "44444" && text2 != "4")
            {
                return true;
            }
            return false;
        }

        public static bool NguQuy_55555(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text3 + text4 + text5 + text6 + text7;
            if (text8 == "55555" && text2 != "5")
            {
                return true;
            }
            return false;
        }

        public static bool NguQuy_66666(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text3 + text4 + text5 + text6 + text7;
            if (text8 == "66666" && text2 != "6")
            {
                return true;
            }
            return false;
        }

        public static bool NguQuy_77777(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text3 + text4 + text5 + text6 + text7;
            if (text8 == "77777" && text2 != "7")
            {
                return true;
            }
            return false;
        }

        public static bool NguQuy_88888(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text3 + text4 + text5 + text6 + text7;
            if (text8 == "88888" && text2 != "8")
            {
                return true;
            }
            return false;
        }

        public static bool NguQuy_99999(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text3 + text4 + text5 + text6 + text7;
            if (text8 == "99999" && text2 != "9")
            {
                return true;
            }
            return false;
        }

        public static bool NguGiua(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4 + text5 + text6;
            string[] source = new string[10] { "00000", "11111", "22222", "33333", "44444", "55555", "66666", "77777", "88888", "99999" };
            if (source.Contains(value) && text7 != text6)
            {
                return true;
            }
            return false;
        }

        public static bool NguGiua_00000(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5 + text6;
            if (text8 == "00000" && text7 != "0")
            {
                return true;
            }
            return false;
        }

        public static bool NguGiua_11111(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5 + text6;
            if (text8 == "11111" && text7 != "1")
            {
                return true;
            }
            return false;
        }

        public static bool NguGiua_22222(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5 + text6;
            if (text8 == "22222" && text7 != "2")
            {
                return true;
            }
            return false;
        }

        public static bool NguGiua_33333(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5 + text6;
            if (text8 == "33333" && text7 != "3")
            {
                return true;
            }
            return false;
        }

        public static bool NguGiua_44444(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5 + text6;
            if (text8 == "44444" && text7 != "4")
            {
                return true;
            }
            return false;
        }

        public static bool NguGiua_55555(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5 + text6;
            if (text8 == "55555" && text7 != "5")
            {
                return true;
            }
            return false;
        }

        public static bool NguGiua_66666(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5 + text6;
            if (text8 == "66666" && text7 != "6")
            {
                return true;
            }
            return false;
        }

        public static bool NguGiua_77777(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5 + text6;
            if (text8 == "77777" && text7 != "7")
            {
                return true;
            }
            return false;
        }

        public static bool NguGiua_88888(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5 + text6;
            if (text8 == "88888" && text7 != "8")
            {
                return true;
            }
            return false;
        }

        public static bool NguGiua_99999(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5 + text6;
            if (text8 == "99999" && text7 != "9")
            {
                return true;
            }
            return false;
        }

        public static bool PhatLoc_x86(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[22]
            {
            "000", "111", "222", "333", "444", "555", "666", "777", "888", "999",
            "668", "686", "688", "868", "886", "866", "234", "345", "456", "567",
            "678", "789"
            };
            string value2 = text5 + text6 + text7;
            string[] source2 = new string[7] { "186", "286", "386", "486", "586", "786", "986" };
            if (source2.Contains(value2) && !source.Contains(value) && !NamSinh_ddmmyy(somay))
            {
                return true;
            }
            return false;
        }

        public static bool Sanh3(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[18]
            {
            "000", "111", "222", "333", "444", "555", "666", "777", "888", "999",
            "012", "123", "234", "345", "456", "567", "678", "789"
            };
            string value2 = text5 + text6 + text7;
            string[] source2 = new string[8] { "012", "123", "234", "345", "456", "567", "678", "789" };
            if (source2.Contains(value2) && Convert.ToInt32(text4) + 1 != Convert.ToInt32(text5) && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool Sanh3_012(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[18]
            {
            "000", "111", "222", "333", "444", "555", "666", "777", "888", "999",
            "012", "123", "234", "345", "456", "567", "678", "789"
            };
            string text8 = text5 + text6 + text7;
            if (text8 == "012" && Convert.ToInt32(text4) + 1 != Convert.ToInt32(text5) && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool Sanh3_123(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[18]
            {
            "000", "111", "222", "333", "444", "555", "666", "777", "888", "999",
            "012", "123", "234", "345", "456", "567", "678", "789"
            };
            string text8 = text5 + text6 + text7;
            if (text8 == "123" && Convert.ToInt32(text4) + 1 != Convert.ToInt32(text5) && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool Sanh3_234(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[18]
            {
            "000", "111", "222", "333", "444", "555", "666", "777", "888", "999",
            "012", "123", "234", "345", "456", "567", "678", "789"
            };
            string text8 = text5 + text6 + text7;
            if (text8 == "234" && Convert.ToInt32(text4) + 1 != Convert.ToInt32(text5) && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool Sanh3_345(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[18]
            {
            "000", "111", "222", "333", "444", "555", "666", "777", "888", "999",
            "012", "123", "234", "345", "456", "567", "678", "789"
            };
            string text8 = text5 + text6 + text7;
            if (text8 == "345" && Convert.ToInt32(text4) + 1 != Convert.ToInt32(text5) && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool Sanh3_456(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[18]
            {
            "000", "111", "222", "333", "444", "555", "666", "777", "888", "999",
            "012", "123", "234", "345", "456", "567", "678", "789"
            };
            string text8 = text5 + text6 + text7;
            if (text8 == "456" && Convert.ToInt32(text4) + 1 != Convert.ToInt32(text5) && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool Sanh3_567(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[18]
            {
            "000", "111", "222", "333", "444", "555", "666", "777", "888", "999",
            "012", "123", "234", "345", "456", "567", "678", "789"
            };
            string text8 = text5 + text6 + text7;
            if (text8 == "567" && Convert.ToInt32(text4) + 1 != Convert.ToInt32(text5) && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool Sanh3_678(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[18]
            {
            "000", "111", "222", "333", "444", "555", "666", "777", "888", "999",
            "012", "123", "234", "345", "456", "567", "678", "789"
            };
            string text8 = text5 + text6 + text7;
            if (text8 == "678" && Convert.ToInt32(text4) + 1 != Convert.ToInt32(text5) && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool Sanh3_789(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[18]
            {
            "000", "111", "222", "333", "444", "555", "666", "777", "888", "999",
            "012", "123", "234", "345", "456", "567", "678", "789"
            };
            string text8 = text5 + text6 + text7;
            if (text8 == "789" && Convert.ToInt32(text4) + 1 != Convert.ToInt32(text5) && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool Tien3Nut_678_789(string somay)
        {
            string pattern = "([0-9]{6})(678$|789$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{6})(678$|789$)";
                    break;
                case 10:
                    pattern = "([0-9]{7})(678$|789$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool Tien3Nut_123_567(string somay)
        {
            string pattern = "([0-9]{6})(123$|234$|345$|456$|567$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{6})(123$|234$|345$|456$|567$)";
                    break;
                case 10:
                    pattern = "([0-9]{7})(123$|234$|345$|456$|567$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool Sanh3_2Cap(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[8] { "012", "123", "234", "345", "456", "567", "678", "789" };
            string value2 = text5 + text6 + text7;
            string[] source2 = new string[8] { "012", "123", "234", "345", "456", "567", "678", "789" };
            if (source.Contains(value) && source2.Contains(value2))
            {
                return true;
            }
            return false;
        }

        public static bool SanhTien3_Dep_123(string somay)
        {
            string[] source = new string[20]
            {
            "686", "866", "868", "668", "886", "688", "000", "111", "222", "333",
            "444", "555", "666", "777", "888", "999", "779", "979", "799", "939"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 == "123")
            {
                return true;
            }
            return false;
        }

        public static bool SanhTien3_Dep_234(string somay)
        {
            string[] source = new string[20]
            {
            "686", "866", "868", "668", "886", "688", "000", "111", "222", "333",
            "444", "555", "666", "777", "888", "999", "779", "979", "799", "939"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 == "234")
            {
                return true;
            }
            return false;
        }

        public static bool SanhTien3_Dep_345(string somay)
        {
            string[] source = new string[20]
            {
            "686", "866", "868", "668", "886", "688", "000", "111", "222", "333",
            "444", "555", "666", "777", "888", "999", "779", "979", "799", "939"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 == "345")
            {
                return true;
            }
            return false;
        }

        public static bool SanhTien3_Dep_456(string somay)
        {
            string[] source = new string[20]
            {
            "686", "866", "868", "668", "886", "688", "000", "111", "222", "333",
            "444", "555", "666", "777", "888", "999", "779", "979", "799", "939"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 == "456")
            {
                return true;
            }
            return false;
        }

        public static bool SanhTien3_Dep_567(string somay)
        {
            string[] source = new string[20]
            {
            "686", "866", "868", "668", "886", "688", "000", "111", "222", "333",
            "444", "555", "666", "777", "888", "999", "779", "979", "799", "939"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 == "567")
            {
                return true;
            }
            return false;
        }

        public static bool SanhTien3_Dep_678(string somay)
        {
            string[] source = new string[20]
            {
            "686", "866", "868", "668", "886", "688", "000", "111", "222", "333",
            "444", "555", "666", "777", "888", "999", "779", "979", "799", "939"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 == "678")
            {
                return true;
            }
            return false;
        }

        public static bool SanhTien3_Dep_789(string somay)
        {
            string[] source = new string[20]
            {
            "686", "866", "868", "668", "886", "688", "000", "111", "222", "333",
            "444", "555", "666", "777", "888", "999", "779", "979", "799", "939"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && text5 + text6 + text7 == "789")
            {
                return true;
            }
            return false;
        }

        public static bool SanhTien3_Dep_1(string somay)
        {
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string[] source2 = new string[7] { "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (source.Contains(text2 + text3 + text4) && source2.Contains(text5 + text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool SanhTien3_Dep_2(string somay)
        {
            string[] source = new string[7] { "123", "234", "345", "456", "567", "678", "789" };
            string[] source2 = new string[14]
            {
            "6686", "6866", "8688", "8868", "6668", "8886", "6886", "8668", "6868", "8686",
            "6688", "8866", "7779", "7979"
            };
            string text = somay.Substring(0, 2);
            string text2 = somay.Substring(2, 1);
            string text3 = somay.Substring(3, 1);
            string text4 = somay.Substring(4, 1);
            string text5 = somay.Substring(5, 1);
            string text6 = somay.Substring(6, 1);
            string text7 = somay.Substring(7, 1);
            string text8 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    text8 = somay.Substring(9, 1);
                    break;
            }
            if (source2.Contains(text2 + text3 + text4 + text5) && source.Contains(text6 + text7 + text8))
            {
                return true;
            }
            return false;
        }

        public static bool Sanh4(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string value = somay.Substring(4, 1);
            string text3 = somay.Substring(5, 1);
            string text4 = somay.Substring(6, 1);
            string text5 = somay.Substring(7, 1);
            string text6 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    value = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    value = somay.Substring(5, 1);
                    text3 = somay.Substring(6, 1);
                    text4 = somay.Substring(7, 1);
                    text5 = somay.Substring(8, 1);
                    text6 = somay.Substring(9, 1);
                    break;
            }
            string value2 = text3 + text4 + text5 + text6;
            string[] source = new string[7] { "0123", "1234", "2345", "3456", "4567", "5678", "6789" };
            if (source.Contains(value2) && Convert.ToInt32(value) + 1 != Convert.ToInt32(text3))
            {
                return true;
            }
            return false;
        }

        public static bool Sanh4_0123(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string value = somay.Substring(4, 1);
            string text3 = somay.Substring(5, 1);
            string text4 = somay.Substring(6, 1);
            string text5 = somay.Substring(7, 1);
            string text6 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    value = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    value = somay.Substring(5, 1);
                    text3 = somay.Substring(6, 1);
                    text4 = somay.Substring(7, 1);
                    text5 = somay.Substring(8, 1);
                    text6 = somay.Substring(9, 1);
                    break;
            }
            string text7 = text3 + text4 + text5 + text6;
            if (text7 == "0123" && Convert.ToInt32(value) + 1 != Convert.ToInt32(text3))
            {
                return true;
            }
            return false;
        }

        public static bool Sanh4_1234(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string value = somay.Substring(4, 1);
            string text3 = somay.Substring(5, 1);
            string text4 = somay.Substring(6, 1);
            string text5 = somay.Substring(7, 1);
            string text6 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    value = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    value = somay.Substring(5, 1);
                    text3 = somay.Substring(6, 1);
                    text4 = somay.Substring(7, 1);
                    text5 = somay.Substring(8, 1);
                    text6 = somay.Substring(9, 1);
                    break;
            }
            string text7 = text3 + text4 + text5 + text6;
            if (text7 == "1234" && Convert.ToInt32(value) + 1 != Convert.ToInt32(text3))
            {
                return true;
            }
            return false;
        }

        public static bool Sanh4_2345(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string value = somay.Substring(4, 1);
            string text3 = somay.Substring(5, 1);
            string text4 = somay.Substring(6, 1);
            string text5 = somay.Substring(7, 1);
            string text6 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    value = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    value = somay.Substring(5, 1);
                    text3 = somay.Substring(6, 1);
                    text4 = somay.Substring(7, 1);
                    text5 = somay.Substring(8, 1);
                    text6 = somay.Substring(9, 1);
                    break;
            }
            string text7 = text3 + text4 + text5 + text6;
            if (text7 == "2345" && Convert.ToInt32(value) + 1 != Convert.ToInt32(text3))
            {
                return true;
            }
            return false;
        }

        public static bool Sanh4_3456(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string value = somay.Substring(4, 1);
            string text3 = somay.Substring(5, 1);
            string text4 = somay.Substring(6, 1);
            string text5 = somay.Substring(7, 1);
            string text6 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    value = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    value = somay.Substring(5, 1);
                    text3 = somay.Substring(6, 1);
                    text4 = somay.Substring(7, 1);
                    text5 = somay.Substring(8, 1);
                    text6 = somay.Substring(9, 1);
                    break;
            }
            string text7 = text3 + text4 + text5 + text6;
            if (text7 == "3456" && Convert.ToInt32(value) + 1 != Convert.ToInt32(text3))
            {
                return true;
            }
            return false;
        }

        public static bool Sanh4_4567(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string value = somay.Substring(4, 1);
            string text3 = somay.Substring(5, 1);
            string text4 = somay.Substring(6, 1);
            string text5 = somay.Substring(7, 1);
            string text6 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    value = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    value = somay.Substring(5, 1);
                    text3 = somay.Substring(6, 1);
                    text4 = somay.Substring(7, 1);
                    text5 = somay.Substring(8, 1);
                    text6 = somay.Substring(9, 1);
                    break;
            }
            string text7 = text3 + text4 + text5 + text6;
            if (text7 == "4567" && Convert.ToInt32(value) + 1 != Convert.ToInt32(text3))
            {
                return true;
            }
            return false;
        }

        public static bool Sanh4_5678(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string value = somay.Substring(4, 1);
            string text3 = somay.Substring(5, 1);
            string text4 = somay.Substring(6, 1);
            string text5 = somay.Substring(7, 1);
            string text6 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    value = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    value = somay.Substring(5, 1);
                    text3 = somay.Substring(6, 1);
                    text4 = somay.Substring(7, 1);
                    text5 = somay.Substring(8, 1);
                    text6 = somay.Substring(9, 1);
                    break;
            }
            string text7 = text3 + text4 + text5 + text6;
            if (text7 == "5678" && Convert.ToInt32(value) + 1 != Convert.ToInt32(text3))
            {
                return true;
            }
            return false;
        }

        public static bool Sanh4_6789(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string value = somay.Substring(4, 1);
            string text3 = somay.Substring(5, 1);
            string text4 = somay.Substring(6, 1);
            string text5 = somay.Substring(7, 1);
            string text6 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    value = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    value = somay.Substring(5, 1);
                    text3 = somay.Substring(6, 1);
                    text4 = somay.Substring(7, 1);
                    text5 = somay.Substring(8, 1);
                    text6 = somay.Substring(9, 1);
                    break;
            }
            string text7 = text3 + text4 + text5 + text6;
            if (text7 == "6789" && Convert.ToInt32(value) + 1 != Convert.ToInt32(text3))
            {
                return true;
            }
            return false;
        }

        public static bool Sanh5(string somay)
        {
            string text = somay.Substring(0, 3);
            string value = somay.Substring(3, 1);
            string text2 = somay.Substring(4, 1);
            string text3 = somay.Substring(5, 1);
            string text4 = somay.Substring(6, 1);
            string text5 = somay.Substring(7, 1);
            string text6 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    value = somay.Substring(3, 1);
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    break;
                case 10:
                    value = somay.Substring(4, 1);
                    text2 = somay.Substring(5, 1);
                    text3 = somay.Substring(6, 1);
                    text4 = somay.Substring(7, 1);
                    text5 = somay.Substring(8, 1);
                    text6 = somay.Substring(9, 1);
                    break;
            }
            string value2 = text2 + text3 + text4 + text5 + text6;
            string[] source = new string[6] { "01234", "12345", "23456", "34567", "45678", "56789" };
            if (source.Contains(value2) && Convert.ToInt32(value) + 1 != Convert.ToInt32(text2))
            {
                return true;
            }
            return false;
        }

        public static bool Sanh5_01234(string somay)
        {
            string text = somay.Substring(0, 3);
            string value = somay.Substring(3, 1);
            string text2 = somay.Substring(4, 1);
            string text3 = somay.Substring(5, 1);
            string text4 = somay.Substring(6, 1);
            string text5 = somay.Substring(7, 1);
            string text6 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    value = somay.Substring(3, 1);
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    break;
                case 10:
                    value = somay.Substring(4, 1);
                    text2 = somay.Substring(5, 1);
                    text3 = somay.Substring(6, 1);
                    text4 = somay.Substring(7, 1);
                    text5 = somay.Substring(8, 1);
                    text6 = somay.Substring(9, 1);
                    break;
            }
            string text7 = text2 + text3 + text4 + text5 + text6;
            if (text7 == "01234" && Convert.ToInt32(value) + 1 != Convert.ToInt32(text2))
            {
                return true;
            }
            return false;
        }

        public static bool Sanh5_12345(string somay)
        {
            string text = somay.Substring(0, 3);
            string value = somay.Substring(3, 1);
            string text2 = somay.Substring(4, 1);
            string text3 = somay.Substring(5, 1);
            string text4 = somay.Substring(6, 1);
            string text5 = somay.Substring(7, 1);
            string text6 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    value = somay.Substring(3, 1);
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    break;
                case 10:
                    value = somay.Substring(4, 1);
                    text2 = somay.Substring(5, 1);
                    text3 = somay.Substring(6, 1);
                    text4 = somay.Substring(7, 1);
                    text5 = somay.Substring(8, 1);
                    text6 = somay.Substring(9, 1);
                    break;
            }
            string text7 = text2 + text3 + text4 + text5 + text6;
            if (text7 == "12345" && Convert.ToInt32(value) + 1 != Convert.ToInt32(text2))
            {
                return true;
            }
            return false;
        }

        public static bool Sanh5_23456(string somay)
        {
            string text = somay.Substring(0, 3);
            string value = somay.Substring(3, 1);
            string text2 = somay.Substring(4, 1);
            string text3 = somay.Substring(5, 1);
            string text4 = somay.Substring(6, 1);
            string text5 = somay.Substring(7, 1);
            string text6 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    value = somay.Substring(3, 1);
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    break;
                case 10:
                    value = somay.Substring(4, 1);
                    text2 = somay.Substring(5, 1);
                    text3 = somay.Substring(6, 1);
                    text4 = somay.Substring(7, 1);
                    text5 = somay.Substring(8, 1);
                    text6 = somay.Substring(9, 1);
                    break;
            }
            string text7 = text2 + text3 + text4 + text5 + text6;
            if (text7 == "23456" && Convert.ToInt32(value) + 1 != Convert.ToInt32(text2))
            {
                return true;
            }
            return false;
        }

        public static bool Sanh5_34567(string somay)
        {
            string text = somay.Substring(0, 3);
            string value = somay.Substring(3, 1);
            string text2 = somay.Substring(4, 1);
            string text3 = somay.Substring(5, 1);
            string text4 = somay.Substring(6, 1);
            string text5 = somay.Substring(7, 1);
            string text6 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    value = somay.Substring(3, 1);
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    break;
                case 10:
                    value = somay.Substring(4, 1);
                    text2 = somay.Substring(5, 1);
                    text3 = somay.Substring(6, 1);
                    text4 = somay.Substring(7, 1);
                    text5 = somay.Substring(8, 1);
                    text6 = somay.Substring(9, 1);
                    break;
            }
            string text7 = text2 + text3 + text4 + text5 + text6;
            if (text7 == "34567" && Convert.ToInt32(value) + 1 != Convert.ToInt32(text2))
            {
                return true;
            }
            return false;
        }

        public static bool Sanh5_45678(string somay)
        {
            string text = somay.Substring(0, 3);
            string value = somay.Substring(3, 1);
            string text2 = somay.Substring(4, 1);
            string text3 = somay.Substring(5, 1);
            string text4 = somay.Substring(6, 1);
            string text5 = somay.Substring(7, 1);
            string text6 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    value = somay.Substring(3, 1);
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    break;
                case 10:
                    value = somay.Substring(4, 1);
                    text2 = somay.Substring(5, 1);
                    text3 = somay.Substring(6, 1);
                    text4 = somay.Substring(7, 1);
                    text5 = somay.Substring(8, 1);
                    text6 = somay.Substring(9, 1);
                    break;
            }
            string text7 = text2 + text3 + text4 + text5 + text6;
            if (text7 == "45678" && Convert.ToInt32(value) + 1 != Convert.ToInt32(text2))
            {
                return true;
            }
            return false;
        }

        public static bool Sanh5_56789(string somay)
        {
            string text = somay.Substring(0, 3);
            string value = somay.Substring(3, 1);
            string text2 = somay.Substring(4, 1);
            string text3 = somay.Substring(5, 1);
            string text4 = somay.Substring(6, 1);
            string text5 = somay.Substring(7, 1);
            string text6 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    value = somay.Substring(3, 1);
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    break;
                case 10:
                    value = somay.Substring(4, 1);
                    text2 = somay.Substring(5, 1);
                    text3 = somay.Substring(6, 1);
                    text4 = somay.Substring(7, 1);
                    text5 = somay.Substring(8, 1);
                    text6 = somay.Substring(9, 1);
                    break;
            }
            string text7 = text2 + text3 + text4 + text5 + text6;
            if (text7 == "456789" && Convert.ToInt32(value) + 1 != Convert.ToInt32(text2))
            {
                return true;
            }
            return false;
        }

        public static bool Sanh6(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4 + text5 + text6 + text7;
            string[] source = new string[5] { "012345", "123456", "234567", "345678", "456789" };
            if (source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool SanhTien_1368(string somay)
        {
            string pattern = "([0-9]{5})(1368$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(1368$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(1368$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool SanhTien_2468(string somay)
        {
            string pattern = "([0-9]{5})(2468$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(2468$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(2468$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool SanhTien_3579(string somay)
        {
            string pattern = "([0-9]{5})(3579$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(3579$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(3579$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool SanhGiua3_LapGanh(string somay)
        {
            string[] source = new string[8] { "012", "123", "234", "345", "456", "567", "678", "789" };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            string value = text2 + text3 + text4;
            if (source.Contains(value) && ((text2 == text3 && text3 != text4) || (text2 == text4 && text3 != text4) || (text3 == text4 && text2 != text3)))
            {
                return true;
            }
            return false;
        }

        public static bool SanhGiua3(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[8] { "012", "123", "234", "345", "456", "567", "678", "789" };
            string value2 = text5 + text6 + text7;
            string[] source2 = new string[18]
            {
            "000", "111", "222", "333", "444", "555", "666", "777", "888", "999",
            "012", "123", "234", "345", "456", "567", "678", "789"
            };
            if (source.Contains(value) && Convert.ToInt32(text4) + 1 != Convert.ToInt32(text5) && !source2.Contains(value2))
            {
                return true;
            }
            return false;
        }

        public static bool SanhGiua3_012(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4;
            string value = text5 + text6 + text7;
            string[] source = new string[18]
            {
            "000", "111", "222", "333", "444", "555", "666", "777", "888", "999",
            "012", "123", "234", "345", "456", "567", "678", "789"
            };
            if (text8 == "012" && text6 != "3" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool SanhGiua3_123(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4;
            string value = text5 + text6 + text7;
            string[] source = new string[18]
            {
            "000", "111", "222", "333", "444", "555", "666", "777", "888", "999",
            "012", "123", "234", "345", "456", "567", "678", "789"
            };
            if (text8 == "123" && text6 != "4" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool SanhGiua3_234(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4;
            string value = text5 + text6 + text7;
            string[] source = new string[18]
            {
            "000", "111", "222", "333", "444", "555", "666", "777", "888", "999",
            "012", "123", "234", "345", "456", "567", "678", "789"
            };
            if (text8 == "234" && text6 != "5" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool SanhGiua3_345(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4;
            string value = text5 + text6 + text7;
            string[] source = new string[18]
            {
            "000", "111", "222", "333", "444", "555", "666", "777", "888", "999",
            "012", "123", "234", "345", "456", "567", "678", "789"
            };
            if (text8 == "345" && text6 != "6" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool SanhGiua3_456(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4;
            string value = text5 + text6 + text7;
            string[] source = new string[18]
            {
            "000", "111", "222", "333", "444", "555", "666", "777", "888", "999",
            "012", "123", "234", "345", "456", "567", "678", "789"
            };
            if (text8 == "456" && text6 != "7" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool SanhGiua3_567(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4;
            string value = text5 + text6 + text7;
            string[] source = new string[18]
            {
            "000", "111", "222", "333", "444", "555", "666", "777", "888", "999",
            "012", "123", "234", "345", "456", "567", "678", "789"
            };
            if (text8 == "567" && text6 != "8" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool SanhGiua3_678(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4;
            string value = text5 + text6 + text7;
            string[] source = new string[18]
            {
            "000", "111", "222", "333", "444", "555", "666", "777", "888", "999",
            "012", "123", "234", "345", "456", "567", "678", "789"
            };
            if (text8 == "678" && text6 != "9" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool SanhGiua3_789(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4;
            string value = text5 + text6 + text7;
            string[] source = new string[18]
            {
            "000", "111", "222", "333", "444", "555", "666", "777", "888", "999",
            "012", "123", "234", "345", "456", "567", "678", "789"
            };
            if (text8 == "789" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool SanhGiua4(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4 + text5;
            string[] source = new string[7] { "0123", "1234", "2345", "3456", "4567", "5678", "6789" };
            string value2 = text5 + text6 + text7;
            string[] source2 = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            if (source.Contains(value) && Convert.ToInt32(text5) + 1 != Convert.ToInt32(text6) && !source2.Contains(value2))
            {
                return true;
            }
            return false;
        }

        public static bool SanhGiua4_0123(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5;
            string value = text5 + text6 + text7;
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            if (text8 == "0123" && text6 != "4" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool SanhGiua4_1234(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5;
            string value = text5 + text6 + text7;
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            if (text8 == "1234" && text6 != "5" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool SanhGiua4_2345(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5;
            string value = text5 + text6 + text7;
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            if (text8 == "2345" && text6 != "6" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool SanhGiua4_3456(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5;
            string value = text5 + text6 + text7;
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            if (text8 == "3456" && text6 != "7" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool SanhGiua4_4567(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5;
            string value = text5 + text6 + text7;
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            if (text8 == "4567" && text6 != "8" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool SanhGiua4_5678(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5;
            string value = text5 + text6 + text7;
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            if (text8 == "5678" && text6 != "9" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool SanhGiua4_6789(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5;
            string value = text5 + text6 + text7;
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            if (text8 == "6789" && text6 + text7 != "99" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool SanhGiua5(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string value = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    value = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    value = somay.Substring(9, 1);
                    break;
            }
            string value2 = text2 + text3 + text4 + text5 + text6;
            string[] source = new string[6] { "01234", "12345", "23456", "34567", "45678", "56789" };
            if (source.Contains(value2) && Convert.ToInt32(text6) + 1 != Convert.ToInt32(value))
            {
                return true;
            }
            return false;
        }

        public static bool SanhGiua5_01234(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5 + text6;
            if (text8 == "01234" && text6 != "5")
            {
                return true;
            }
            return false;
        }

        public static bool SanhGiua5_12345(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5 + text6;
            if (text8 == "12345" && text6 != "6")
            {
                return true;
            }
            return false;
        }

        public static bool SanhGiua5_23456(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5 + text6;
            if (text8 == "23456" && text6 != "7")
            {
                return true;
            }
            return false;
        }

        public static bool SanhGiua5_34567(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5 + text6;
            if (text8 == "34567" && text6 != "8")
            {
                return true;
            }
            return false;
        }

        public static bool SanhGiua5_45678(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5 + text6;
            if (text8 == "45678" && text6 != "9")
            {
                return true;
            }
            return false;
        }

        public static bool SanhGiua5_56789(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5 + text6;
            if (text8 == "56789")
            {
                return true;
            }
            return false;
        }

        public static bool ABBA(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text4 == text7 && text5 == text6 && text4 != text5)
            {
                string[] source = new string[21]
                {
                "9009", "9119", "9229", "9339", "9559", "9669", "9779", "9889", "8008", "8118",
                "8228", "8338", "8558", "8778", "8998", "6006", "6116", "6226", "6336", "6556",
                "6996"
                };
                if (source.Contains(text4 + text5 + text6 + text7))
                {
                    return true;
                }
            }
            return false;
        }

        public static bool ABBA_All(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text4 == text7 && text5 == text6 && text4 != text5 && !LocPhat4(somay) && !NamSinh(somay) && !TamGiua(somay))
            {
                return true;
            }
            return false;
        }

        public static bool SoDao_9669(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3;
            string text9 = text4 + text5 + text6 + text7;
            if (text9 == "9669" && text8 != "96" && text8 != "69" && text8 != "99" && text8 != "39" && text8 != "88" && text8 != "89" && text8 != "98" && text8 != "66")
            {
                return true;
            }
            return false;
        }

        public static bool SoDao_9779(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3;
            string text9 = text4 + text5 + text6 + text7;
            if (text9 == "9779" && text8 != "97" && text8 != "79" && text8 != "99" && text8 != "39" && text8 != "77" && text8 != "88")
            {
                return true;
            }
            return false;
        }

        public static bool SoDao_9889(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3;
            string text9 = text4 + text5 + text6 + text7;
            if (text9 == "9889" && text8 != "98" && text8 != "89" && text8 != "99" && text8 != "39" && text8 != "88" && text8 != "77")
            {
                return true;
            }
            return false;
        }

        public static bool SoDao_8998(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3;
            string text9 = text4 + text5 + text6 + text7;
            if (text9 == "8998" && text8 != "98" && text8 != "89" && text8 != "99" && text8 != "39" && text8 != "88" && text8 != "77")
            {
                return true;
            }
            return false;
        }

        public static bool SoDao_7997(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3;
            string text9 = text4 + text5 + text6 + text7;
            if (text9 == "7997" && text8 != "97" && text8 != "79" && text8 != "99" && text8 != "77" && text8 != "88")
            {
                return true;
            }
            return false;
        }

        public static bool SoDoc_1102(string somay)
        {
            string pattern = "([0-9]{5})(1102$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(1102$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(1102$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool SoDoc_4078(string somay)
        {
            string pattern = "([0-9]{5})(4078$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(4078$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(4078$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool SoDoc_6Nut(string somay)
        {
            string pattern = "([0-9]{3})(049053$|151618$|365078$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{3})(049053$|151618$|365078$)";
                    break;
                case 10:
                    pattern = "([0-9]{4})(049053$|151618$|365078$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool AABB(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text4 == text5 && text6 == text7 && text5 != text6)
            {
                string[] source = new string[21]
                {
                "0099", "1199", "2299", "3399", "5599", "6699", "7799", "8899", "0088", "1188",
                "2288", "3388", "5588", "7788", "9988", "1166", "2266", "3366", "5566", "9966",
                "8833"
                };
                if (source.Contains(text4 + text5 + text6 + text7))
                {
                    return true;
                }
            }
            return false;
        }

        public static bool BBAA(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text4 == text5 && text6 == text7 && Convert.ToInt32(text5) > Convert.ToInt32(text6) && text2 != text3)
            {
                return true;
            }
            return false;
        }

        public static bool AABBCC(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text2 == text3 && text4 == text5 && text6 == text7 && text2 != text4 && text2 != text6 && text4 != text7)
            {
                return true;
            }
            return false;
        }

        public static bool AABBCC_TienCach(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3;
            string value2 = text4 + text5;
            string value3 = text6 + text7;
            if (text2 == text3 && text4 == text5 && text6 == text7 && Convert.ToInt32(value) < Convert.ToInt32(value2) && Convert.ToInt32(value2) < Convert.ToInt32(value3))
            {
                return true;
            }
            return false;
        }

        public static bool AABBCC(string somay, string[] duoiso_filter)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            if (text2 == text3 && text4 == text5 && text6 == text7 && duoiso_filter.Contains(text2 + text3 + text4 + text5 + text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool SoKep_1166(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3;
            string text9 = text4 + text5 + text6 + text7;
            if (text9 == "1166" && text8 != "11" && text8 != "66" && text8 != "16" && text8 != "61")
            {
                return true;
            }
            return false;
        }

        public static bool SoKep_1188(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3;
            string text9 = text4 + text5 + text6 + text7;
            if (text9 == "1188" && text8 != "11" && text8 != "88" && text8 != "18" && text8 != "81")
            {
                return true;
            }
            return false;
        }

        public static bool SoKep_1199(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3;
            string text9 = text4 + text5 + text6 + text7;
            if (text9 == "1199" && text8 != "11" && text8 != "99" && text8 != "19" && text8 != "91")
            {
                return true;
            }
            return false;
        }

        public static bool SoKep_2266(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3;
            string text9 = text4 + text5 + text6 + text7;
            if (text9 == "2266" && text8 != "22" && text8 != "66" && text8 != "26" && text8 != "62")
            {
                return true;
            }
            return false;
        }

        public static bool SoKep_2288(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3;
            string text9 = text4 + text5 + text6 + text7;
            if (text9 == "2288" && text8 != "22" && text8 != "88" && text8 != "28" && text8 != "82")
            {
                return true;
            }
            return false;
        }

        public static bool SoKep_2299(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3;
            string text9 = text4 + text5 + text6 + text7;
            if (text9 == "2299" && text8 != "22" && text8 != "99" && text8 != "29" && text8 != "92")
            {
                return true;
            }
            return false;
        }

        public static bool SoKep_3366(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3;
            string text9 = text4 + text5 + text6 + text7;
            if (text9 == "3366" && text8 != "33" && text8 != "66" && text8 != "36" && text8 != "63")
            {
                return true;
            }
            return false;
        }

        public static bool SoKep_3388(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3;
            string text9 = text4 + text5 + text6 + text7;
            if (text9 == "3388" && text8 != "33" && text8 != "88" && text8 != "38" && text8 != "83")
            {
                return true;
            }
            return false;
        }

        public static bool SoKep_5566(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3;
            string text9 = text4 + text5 + text6 + text7;
            if (text9 == "5566" && text8 != "55" && text8 != "66" && text8 != "56" && text8 != "65")
            {
                return true;
            }
            return false;
        }

        public static bool SoKep_5588(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3;
            string text9 = text4 + text5 + text6 + text7;
            if (text9 == "5588" && text8 != "55" && text8 != "88" && text8 != "58" && text8 != "85")
            {
                return true;
            }
            return false;
        }

        public static bool SoKep_5599(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3;
            string text9 = text4 + text5 + text6 + text7;
            if (text9 == "5599" && text8 != "55" && text8 != "99" && text8 != "59" && text8 != "95")
            {
                return true;
            }
            return false;
        }

        public static bool SoKep_9988(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3;
            string text9 = text4 + text5 + text6 + text7;
            if (text9 == "9988" && text8 != "99" && text8 != "88" && text8 != "98" && text8 != "89")
            {
                return true;
            }
            return false;
        }

        public static bool ABAB(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text4 == text6 && text5 == text7 && text4 != text5)
            {
                string[] source = new string[32]
                {
                "0909", "1919", "2929", "5959", "6969", "8989", "0808", "1818", "2828", "3838",
                "5858", "7878", "9898", "3737", "4747", "8787", "9797", "1616", "2626", "3636",
                "5656", "9696", "3535", "8585", "9595", "6363", "8383", "9393", "9292", "8282",
                "8181", "9191"
                };
                if (source.Contains(text4 + text5 + text6 + text7))
                {
                    return true;
                }
            }
            return false;
        }

        public static bool ABAB_All(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text4 == text6 && text5 == text7 && Convert.ToInt32(text4) < Convert.ToInt32(text5) && !LocPhat4(somay) && !ThanTai4(somay) && !NamSinh(somay) && !TamGiua(somay))
            {
                return true;
            }
            return false;
        }

        public static bool BABA(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text4 == text6 && text5 == text7 && text4 != text5)
            {
                string[] source = new string[33]
                {
                "0909", "1919", "2929", "3939", "5959", "6969", "8989", "0808", "1818", "2828",
                "3838", "5858", "7878", "9898", "3737", "4747", "8787", "9797", "1616", "2626",
                "3636", "5656", "9696", "3535", "8585", "9595", "6363", "8383", "9393", "9292",
                "8282", "8181", "9191"
                };
                if (source.Contains(text4 + text5 + text6 + text7))
                {
                    return true;
                }
            }
            return false;
        }

        public static bool BABA_All(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text4 == text6 && text5 == text7 && Convert.ToInt32(text4) > Convert.ToInt32(text5) && !LocPhat4(somay) && !ThanTai4(somay) && !NamSinh(somay) && !TamGiua(somay))
            {
                return true;
            }
            return false;
        }

        public static bool SoLap_1616(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3;
            string text9 = text4 + text5 + text6 + text7;
            if (text9 == "1616" && text8 != "16" && text8 != "61")
            {
                return true;
            }
            return false;
        }

        public static bool SoLap_1818(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3;
            string text9 = text4 + text5 + text6 + text7;
            if (text9 == "1818" && text8 != "18" && text8 != "81")
            {
                return true;
            }
            return false;
        }

        public static bool SoLap_1919(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3;
            string text9 = text4 + text5 + text6 + text7;
            if (text9 == "1919" && text8 != "19" && text8 != "91")
            {
                return true;
            }
            return false;
        }

        public static bool SoLap_2626(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3;
            string text9 = text4 + text5 + text6 + text7;
            if (text9 == "2626" && text8 != "26" && text8 != "62")
            {
                return true;
            }
            return false;
        }

        public static bool SoLap_2828(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3;
            string text9 = text4 + text5 + text6 + text7;
            if (text9 == "2828" && text8 != "28" && text8 != "82")
            {
                return true;
            }
            return false;
        }

        public static bool SoLap_2929(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3;
            string text9 = text4 + text5 + text6 + text7;
            if (text9 == "2929" && text8 != "29" && text8 != "92")
            {
                return true;
            }
            return false;
        }

        public static bool SoLap_3636(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3;
            string text9 = text4 + text5 + text6 + text7;
            if (text9 == "3636" && text8 != "36" && text8 != "63")
            {
                return true;
            }
            return false;
        }

        public static bool SoLap_3838(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3;
            string text9 = text4 + text5 + text6 + text7;
            if (text9 == "3838" && text8 != "38" && text8 != "83")
            {
                return true;
            }
            return false;
        }

        public static bool SoLap_5656(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3;
            string text9 = text4 + text5 + text6 + text7;
            if (text9 == "5656" && text8 != "56" && text8 != "65")
            {
                return true;
            }
            return false;
        }

        public static bool SoLap_5858(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3;
            string text9 = text4 + text5 + text6 + text7;
            if (text9 == "5858" && text8 != "58" && text8 != "85")
            {
                return true;
            }
            return false;
        }

        public static bool SoLap_5959(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3;
            string text9 = text4 + text5 + text6 + text7;
            if (text9 == "5959" && text8 != "59" && text8 != "95")
            {
                return true;
            }
            return false;
        }

        public static bool SoLap_9696(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3;
            string text9 = text4 + text5 + text6 + text7;
            if (text9 == "9696" && text8 != "96" && text8 != "69")
            {
                return true;
            }
            return false;
        }

        public static bool SoLap_9898(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3;
            string text9 = text4 + text5 + text6 + text7;
            if (text9 == "9898" && text8 != "98" && text8 != "89")
            {
                return true;
            }
            return false;
        }

        public static bool Tien1(string somay)
        {
            string text = somay.Substring(0, 3);
            string value = somay.Substring(3, 2);
            string value2 = somay.Substring(5, 2);
            string value3 = somay.Substring(7, 2);
            switch (somay.Length)
            {
                case 9:
                    value = somay.Substring(3, 2);
                    value2 = somay.Substring(5, 2);
                    value3 = somay.Substring(7, 2);
                    break;
                case 10:
                    value = somay.Substring(4, 2);
                    value2 = somay.Substring(6, 2);
                    value3 = somay.Substring(8, 2);
                    break;
            }
            if (Convert.ToInt32(value) + 1 == Convert.ToInt32(value2) && Convert.ToInt32(value2) + 1 == Convert.ToInt32(value3))
            {
                return true;
            }
            return false;
        }

        public static bool Tien10(string somay)
        {
            string text = somay.Substring(0, 3);
            string value = somay.Substring(3, 2);
            string value2 = somay.Substring(5, 2);
            string value3 = somay.Substring(7, 2);
            switch (somay.Length)
            {
                case 9:
                    value = somay.Substring(3, 2);
                    value2 = somay.Substring(5, 2);
                    value3 = somay.Substring(7, 2);
                    break;
                case 10:
                    value = somay.Substring(4, 2);
                    value2 = somay.Substring(6, 2);
                    value3 = somay.Substring(8, 2);
                    break;
            }
            if (Convert.ToInt32(value) + 10 == Convert.ToInt32(value2) && Convert.ToInt32(value2) + 10 == Convert.ToInt32(value3))
            {
                return true;
            }
            return false;
        }

        public static bool Tien_Cach_Dau_a52b52(string somay)
        {
            string text = somay.Substring(0, 3);
            string value = somay.Substring(3, 1);
            string text2 = somay.Substring(4, 1);
            string text3 = somay.Substring(5, 1);
            string value2 = somay.Substring(6, 1);
            string text4 = somay.Substring(7, 1);
            string text5 = somay.Substring(8, 1);
            string text6 = text2 + text3;
            string text7 = text4 + text5;
            if (text6 == "52" && text7 == "52" && Convert.ToInt32(value) + 1 < Convert.ToInt32(value2))
            {
                return true;
            }
            return false;
        }

        public static bool Tien_Cach_Dau(string somay)
        {
            string text = somay.Substring(0, 3);
            string value = somay.Substring(3, 1);
            string text2 = somay.Substring(4, 1);
            string text3 = somay.Substring(5, 1);
            string value2 = somay.Substring(6, 1);
            string text4 = somay.Substring(7, 1);
            string text5 = somay.Substring(8, 1);
            if (text2 + text3 == text4 + text5 && text4 != text5 && Convert.ToInt32(value) + 1 < Convert.ToInt32(value2))
            {
                return true;
            }
            return false;
        }

        public static bool Tien_Cach_Giua(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string value = somay.Substring(4, 1);
            string text3 = somay.Substring(5, 1);
            string text4 = somay.Substring(6, 1);
            string text5 = somay.Substring(7, 1);
            string text6 = somay.Substring(8, 1);
            if (text2 + text3 == text4 + text6 && text4 != text5 && Convert.ToInt32(value) + 1 < Convert.ToInt32(text5))
            {
                return true;
            }
            return false;
        }

        public static bool Tien_Cach_Cuoi(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string value = somay.Substring(5, 1);
            string text4 = somay.Substring(6, 1);
            string text5 = somay.Substring(7, 1);
            string text6 = somay.Substring(8, 1);
            if (text2 + text3 == text4 + text5 && text5 != text6 && Convert.ToInt32(value) + 1 < Convert.ToInt32(text6))
            {
                return true;
            }
            return false;
        }

        public static bool Tien_1_Dau(string somay)
        {
            string text = somay.Substring(0, 3);
            string value = somay.Substring(3, 1);
            string text2 = somay.Substring(4, 1);
            string text3 = somay.Substring(5, 1);
            string text4 = somay.Substring(6, 1);
            string text5 = somay.Substring(7, 1);
            string text6 = somay.Substring(8, 1);
            if (text2 + text3 == text5 + text6 && text4 != text5 && Convert.ToInt32(value) + 1 == Convert.ToInt32(text4))
            {
                return true;
            }
            return false;
        }

        public static bool Tien_1_Giua(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string value = somay.Substring(4, 1);
            string text3 = somay.Substring(5, 1);
            string text4 = somay.Substring(6, 1);
            string text5 = somay.Substring(7, 1);
            string text6 = somay.Substring(8, 1);
            if (text2 + text3 == text4 + text6 && text4 != text5 && Convert.ToInt32(value) + 1 == Convert.ToInt32(text5))
            {
                return true;
            }
            return false;
        }

        public static bool Tien_1_Cuoi(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string value = somay.Substring(5, 1);
            string text4 = somay.Substring(6, 1);
            string text5 = somay.Substring(7, 1);
            string text6 = somay.Substring(8, 1);
            if (text2 + text3 == text4 + text5 && text5 != text6 && Convert.ToInt32(value) + 1 == Convert.ToInt32(text6))
            {
                return true;
            }
            return false;
        }

        public static bool Tien6SoCuoi(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string value = somay.Substring(6, 1);
            string value2 = somay.Substring(7, 1);
            string value3 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    value = somay.Substring(6, 1);
                    value2 = somay.Substring(7, 1);
                    value3 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    value = somay.Substring(7, 1);
                    value2 = somay.Substring(8, 1);
                    value3 = somay.Substring(9, 1);
                    break;
            }
            string text5 = text2 + text3 + text4;
            if (Convert.ToInt32(text3) >= Convert.ToInt32(text2) && Convert.ToInt32(text4) >= Convert.ToInt32(text3) && Convert.ToInt32(value) >= Convert.ToInt32(text4) && Convert.ToInt32(value2) >= Convert.ToInt32(value) && Convert.ToInt32(value3) >= Convert.ToInt32(value2) && !Sanh3_2Cap(somay) && !Sanh3(somay) && !SanhGiua3(somay) && !SanhGiua4(somay) && !x_aaaab(somay) && !xy_aaab(somay))
            {
                return true;
            }
            return false;
        }

        public static bool DuoiSoTo(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[6] { "666", "777", "888", "999", "678", "789" };
            string value2 = text5 + text6 + text7;
            string[] source2 = new string[10] { "668", "686", "688", "886", "866", "868", "779", "789", "799", "979" };
            if (Convert.ToInt32(text2) > 5 && Convert.ToInt32(text3) > 5 && Convert.ToInt32(text4) > 5 && Convert.ToInt32(text5) > 5 && Convert.ToInt32(text6) > 5 && Convert.ToInt32(text7) > 5 && !source.Contains(value) && !source2.Contains(value2) && !source.Contains(value2) && !Tien_Cach_Dau(somay) && !Tien_Cach_Giua(somay) && !Tien_Cach_Cuoi(somay))
            {
                return true;
            }
            return false;
        }

        public static bool TamQuy(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string value2 = text5 + text6 + text7;
            string[] source2 = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            if (source2.Contains(value2) && !source.Contains(value) && text4 != text5)
            {
                return true;
            }
            return false;
        }

        public static bool TamQuy_999(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text8 = text5 + text6 + text7;
            if (text8 == "999" && text4 != "9" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool TamQuy_888(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text8 = text5 + text6 + text7;
            if (text8 == "888" && text4 != "8" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool TamQuy_777(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text8 = text5 + text6 + text7;
            if (text8 == "777" && text4 != "7" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool TamQuy_666(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text8 = text5 + text6 + text7;
            if (text8 == "666" && text4 != "6" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool TamQuy_555(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text8 = text5 + text6 + text7;
            if (text8 == "555" && text4 != "5" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool TamQuy_444(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text8 = text5 + text6 + text7;
            if (text8 == "444" && text4 != "4" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool TamQuy_333(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text8 = text5 + text6 + text7;
            if (text8 == "333" && text4 != "3" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool TamQuy_222(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text8 = text5 + text6 + text7;
            if (text8 == "222" && text4 != "2" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool TamQuy_111(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text8 = text5 + text6 + text7;
            if (text8 == "111" && text4 != "1" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool TamQuy_000(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string text8 = text5 + text6 + text7;
            if (text8 == "000" && text4 != "0" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool TamQuy_aab999(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text2 == text3 && text3 != text4 && text5 + text6 + text7 == "999")
            {
                return true;
            }
            return false;
        }

        public static bool TamQuy_aab888(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text2 == text3 && text3 != text4 && text5 + text6 + text7 == "888")
            {
                return true;
            }
            return false;
        }

        public static bool TamQuy_77a777(string somay)
        {
            string pattern = "([0-9]{3})(77(.*?)777$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{3})(77(.*?)777$)";
                    break;
                case 10:
                    pattern = "([0-9]{4})(77(.*?)777$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool TamQuy_aab666(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text2 == text3 && text3 != text4 && text5 + text6 + text7 == "666")
            {
                return true;
            }
            return false;
        }

        public static bool TamQuy_55a555(string somay)
        {
            string pattern = "([0-9]{3})(55(.*?)555$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{3})(55(.*?)555$)";
                    break;
                case 10:
                    pattern = "([0-9]{4})(55(.*?)555$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool TamQuy_33a333(string somay)
        {
            string pattern = "([0-9]{3})(33(.*?)333$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{3})(33(.*?)333$)";
                    break;
                case 10:
                    pattern = "([0-9]{4})(33(.*?)333$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool TamQuy_22a222(string somay)
        {
            string pattern = "([0-9]{3})(22(.*?)222$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{3})(22(.*?)222$)";
                    break;
                case 10:
                    pattern = "([0-9]{4})(22(.*?)222$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool TamQuy_aba999(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text2 == text4 && text3 != text4 && text5 + text6 + text7 == "999")
            {
                return true;
            }
            return false;
        }

        public static bool TamQuy_aba888(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text2 == text4 && text3 != text4 && text5 + text6 + text7 == "888")
            {
                return true;
            }
            return false;
        }

        public static bool TamQuy_aba666(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text2 == text4 && text3 != text4 && text5 + text6 + text7 == "666")
            {
                return true;
            }
            return false;
        }

        public static bool AAABBB(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text2 == text3 && text3 == text4 && text5 == text6 && text6 == text7 && text2 != text5)
            {
                return true;
            }
            return false;
        }

        public static bool TamGiua(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            string value2 = text5 + text6 + text7;
            string[] source2 = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            if (source.Contains(value) && text5 != text4 && !source2.Contains(value2))
            {
                return true;
            }
            return false;
        }

        public static bool TamGiua_999(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4;
            string value = text5 + text6 + text7;
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            if (text8 == "999" && text5 != "9" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool TamGiua_888(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4;
            string value = text5 + text6 + text7;
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            if (text8 == "888" && text5 != "8" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool TamGiua_777(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4;
            string value = text5 + text6 + text7;
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            if (text8 == "777" && text5 != "7" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool TamGiua_666(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4;
            string value = text5 + text6 + text7;
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            if (text8 == "666" && text5 != "6" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool TamGiua_555(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4;
            string value = text5 + text6 + text7;
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            if (text8 == "555" && text5 != "5" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool TamGiua_444(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4;
            string value = text5 + text6 + text7;
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            if (text8 == "444" && text5 != "4" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool TamGiua_333(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4;
            string value = text5 + text6 + text7;
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            if (text8 == "333" && text5 != "3" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool TamGiua_222(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4;
            string value = text5 + text6 + text7;
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            if (text8 == "222" && text5 != "2" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool TamGiua_111(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4;
            string value = text5 + text6 + text7;
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            if (text8 == "111" && text5 != "1" && !source.Contains(value) && !NamSinh_ddmmyy(somay))
            {
                return true;
            }
            return false;
        }

        public static bool TamGiua_000(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4;
            string value = text5 + text6 + text7;
            string[] source = new string[10] { "000", "111", "222", "333", "444", "555", "666", "777", "888", "999" };
            if (text8 == "000" && text5 != "0" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool TamGiua_689_LapGanh(string somay)
        {
            string[] source = new string[3] { "666", "888", "999" };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            if (source.Contains(value) && ((text2 == text3 && text3 != text4) || (text2 == text4 && text3 != text4) || (text3 == text4 && text2 != text3)))
            {
                return true;
            }
            return false;
        }

        public static bool TamGiua_666_LapGanh(string somay)
        {
            string[] source = new string[1] { "666" };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            if (source.Contains(value) && ((text2 == text3 && text3 != text4) || (text2 == text4 && text3 != text4) || (text3 == text4 && text2 != text3)))
            {
                return true;
            }
            return false;
        }

        public static bool TamGiua_666_aab(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string[] source = new string[2] { "6", "8" };
            string[] source2 = new string[1] { "79" };
            string text8 = text2 + text3 + text4;
            if (text8 == "666" && text5 == text6 && text5 != text7 && !source.Contains(text5) && !source2.Contains(text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool TamGiua_666_aba(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string[] source = new string[1] { "6" };
            string[] source2 = new string[4] { "979", "868", "989", "898" };
            string text8 = text2 + text3 + text4;
            if (text8 == "666" && text5 == text7 && text5 != text6 && !source.Contains(text5) && !source2.Contains(text5 + text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool TamGiua_666_abb(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string[] source = new string[2] { "6", "8" };
            string[] source2 = new string[3] { "66", "88", "99" };
            string text8 = text2 + text3 + text4;
            if (text8 == "666" && text6 == text7 && text5 != text6 && !source.Contains(text5) && !source2.Contains(text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool TamGiua_777_aab(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string[] source = new string[1] { "7" };
            string[] source2 = new string[2] { "68", "79" };
            string text8 = text2 + text3 + text4;
            if (text8 == "777" && text5 == text6 && text5 != text7 && !source.Contains(text5) && !source2.Contains(text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool TamGiua_777_aba(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string[] source = new string[1] { "7" };
            string[] source2 = new string[4] { "979", "868", "989", "898" };
            string text8 = text2 + text3 + text4;
            if (text8 == "777" && text5 == text7 && text5 != text6 && !source.Contains(text5) && !source2.Contains(text5 + text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool TamGiua_777_abb(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string[] source = new string[1] { "7" };
            string[] source2 = new string[4] { "66", "77", "88", "99" };
            string text8 = text2 + text3 + text4;
            if (text8 == "777" && text6 == text7 && text5 != text6 && !source.Contains(text5) && !source2.Contains(text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool TamGiua_888_aab(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string[] source = new string[2] { "6", "8" };
            string[] source2 = new string[1] { "79" };
            string text8 = text2 + text3 + text4;
            if (text8 == "888" && text5 == text6 && text5 != text7 && !source.Contains(text5) && !source2.Contains(text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool TamGiua_888_aba(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string[] source = new string[1] { "8" };
            string[] source2 = new string[5] { "686", "979", "868", "989", "898" };
            string text8 = text2 + text3 + text4;
            if (text8 == "888" && text5 == text7 && text5 != text6 && !source.Contains(text5) && !source2.Contains(text5 + text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool TamGiua_888_abb(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string[] source = new string[2] { "6", "8" };
            string[] source2 = new string[3] { "66", "88", "99" };
            string text8 = text2 + text3 + text4;
            if (text8 == "888" && text6 == text7 && text5 != text6 && !source.Contains(text5) && !source2.Contains(text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool TamGiua_999_aab(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string[] source = new string[1] { "9" };
            string[] source2 = new string[2] { "68", "79" };
            string text8 = text2 + text3 + text4;
            if (text8 == "999" && text5 == text6 && text5 != text7 && !source.Contains(text5) && !source2.Contains(text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool TamGiua_999_aba(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string[] source = new string[1] { "9" };
            string[] source2 = new string[4] { "686", "797", "868", "898" };
            string text8 = text2 + text3 + text4;
            if (text8 == "999" && text5 == text7 && text5 != text6 && !source.Contains(text5) && !source2.Contains(text5 + text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool TamGiua_999_abb(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string[] source = new string[1] { "9" };
            string[] source2 = new string[3] { "66", "88", "99" };
            string text8 = text2 + text3 + text4;
            if (text8 == "999" && text6 == text7 && text5 != text6 && !source.Contains(text5) && !source2.Contains(text6 + text7))
            {
                return true;
            }
            return false;
        }

        public static bool TamGiua_888_LapGanh(string somay)
        {
            string[] source = new string[1] { "888" };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            if (source.Contains(value) && ((text2 == text3 && text3 != text4) || (text2 == text4 && text3 != text4) || (text3 == text4 && text2 != text3)))
            {
                return true;
            }
            return false;
        }

        public static bool TamGiua_999_LapGanh(string somay)
        {
            string[] source = new string[1] { "999" };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            if (source.Contains(value) && ((text2 == text3 && text3 != text4) || (text2 == text4 && text3 != text4) || (text3 == text4 && text2 != text3)))
            {
                return true;
            }
            return false;
        }

        public static bool TamGiua_2357_LapGanh(string somay)
        {
            string[] source = new string[4] { "222", "333", "555", "777" };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            if (source.Contains(value) && ((text2 == text3 && text3 != text4) || (text2 == text4 && text3 != text4) || (text3 == text4 && text2 != text3)))
            {
                return true;
            }
            return false;
        }

        public static bool TamGiua_014_LapGanh(string somay)
        {
            string[] source = new string[3] { "000", "111", "444" };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            if (source.Contains(value) && ((text2 == text3 && text3 != text4) || (text2 == text4 && text3 != text4) || (text3 == text4 && text2 != text3)))
            {
                return true;
            }
            return false;
        }

        public static bool TamGiua_666_LocPhat(string somay)
        {
            string[] source = new string[1] { "666" };
            string[] source2 = new string[17]
            {
            "168", "268", "368", "468", "568", "668", "768", "868", "968", "186",
            "286", "386", "486", "586", "786", "886", "986"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string value2 = text5 + text6 + text7;
            if (source.Contains(value) && source2.Contains(value2))
            {
                return true;
            }
            return false;
        }

        public static bool TamGiua_888_LocPhat(string somay)
        {
            string[] source = new string[1] { "888" };
            string[] source2 = new string[17]
            {
            "168", "268", "368", "468", "568", "668", "768", "868", "968", "186",
            "286", "386", "486", "586", "786", "886", "986"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string value2 = text5 + text6 + text7;
            if (source.Contains(value) && source2.Contains(value2))
            {
                return true;
            }
            return false;
        }

        public static bool TamGiua_999_LocPhat(string somay)
        {
            string[] source = new string[1] { "999" };
            string[] source2 = new string[17]
            {
            "168", "268", "368", "468", "568", "668", "768", "868", "968", "186",
            "286", "386", "486", "586", "786", "886", "986"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string value2 = text5 + text6 + text7;
            if (source.Contains(value) && source2.Contains(value2))
            {
                return true;
            }
            return false;
        }

        public static bool TamGiua_2357_LocPhat(string somay)
        {
            string[] source = new string[4] { "222", "333", "555", "777" };
            string[] source2 = new string[17]
            {
            "168", "268", "368", "468", "568", "668", "768", "868", "968", "186",
            "286", "386", "486", "586", "786", "886", "986"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string value2 = text5 + text6 + text7;
            if (source.Contains(value) && source2.Contains(value2))
            {
                return true;
            }
            return false;
        }

        public static bool TamGiua_014_LocPhat(string somay)
        {
            string[] source = new string[3] { "000", "111", "444" };
            string[] source2 = new string[17]
            {
            "168", "268", "368", "468", "568", "668", "768", "868", "968", "186",
            "286", "386", "486", "586", "786", "886", "986"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string value2 = text5 + text6 + text7;
            if (source.Contains(value) && source2.Contains(value2))
            {
                return true;
            }
            return false;
        }

        public static bool TamGiua_666_ThanTai(string somay)
        {
            string[] source = new string[1] { "666" };
            string[] source2 = new string[14]
            {
            "179", "279", "379", "479", "579", "679", "779", "879", "979", "139",
            "239", "339", "839", "939"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string value2 = text5 + text6 + text7;
            if (source.Contains(value) && source2.Contains(value2))
            {
                return true;
            }
            return false;
        }

        public static bool TamGiua_888_ThanTai(string somay)
        {
            string[] source = new string[1] { "888" };
            string[] source2 = new string[14]
            {
            "179", "279", "379", "479", "579", "679", "779", "879", "979", "139",
            "239", "339", "839", "939"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string value2 = text5 + text6 + text7;
            if (source.Contains(value) && source2.Contains(value2))
            {
                return true;
            }
            return false;
        }

        public static bool TamGiua_999_ThanTai(string somay)
        {
            string[] source = new string[1] { "999" };
            string[] source2 = new string[14]
            {
            "179", "279", "379", "479", "579", "679", "779", "879", "979", "139",
            "239", "339", "839", "939"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string value2 = text5 + text6 + text7;
            if (source.Contains(value) && source2.Contains(value2))
            {
                return true;
            }
            return false;
        }

        public static bool TamGiua_2357_ThanTai(string somay)
        {
            string[] source = new string[4] { "222", "333", "555", "777" };
            string[] source2 = new string[14]
            {
            "179", "279", "379", "479", "579", "679", "779", "879", "979", "139",
            "239", "339", "839", "939"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            string value = text2 + text3 + text4;
            string value2 = text5 + text6 + text7;
            if (source.Contains(value) && source2.Contains(value2))
            {
                return true;
            }
            return false;
        }

        public static bool TamGiua_014_ThanTai(string somay)
        {
            string[] source = new string[3] { "000", "111", "444" };
            string[] source2 = new string[14]
            {
            "179", "279", "379", "479", "579", "679", "779", "879", "979", "139",
            "239", "339", "839", "939"
            };
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            string value = text2 + text3 + text4;
            string value2 = text5 + text6 + text7;
            if (source.Contains(value) && source2.Contains(value2))
            {
                return true;
            }
            return false;
        }

        public static bool ABABAB(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text2 == text4 && text4 == text6 && text3 == text5 && text5 == text7 && text2 != text3)
            {
                return true;
            }
            return false;
        }

        public static bool AABBAA(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3;
            string text9 = text4 + text5;
            string text10 = text6 + text7;
            if (text2 == text3 && text4 == text5 && text6 == text7 && text8 == text10 && text3 != text4)
            {
                return true;
            }
            return false;
        }

        public static bool ABCABCABC(string somay)
        {
            string text = somay.Substring(0, 1);
            string text2 = somay.Substring(1, 1);
            string text3 = somay.Substring(2, 1);
            string text4 = somay.Substring(3, 1);
            string text5 = somay.Substring(4, 1);
            string text6 = somay.Substring(5, 1);
            string text7 = somay.Substring(6, 1);
            string text8 = somay.Substring(7, 1);
            string text9 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text = somay.Substring(0, 1);
                    text2 = somay.Substring(1, 1);
                    text3 = somay.Substring(2, 1);
                    text4 = somay.Substring(3, 1);
                    text5 = somay.Substring(4, 1);
                    text6 = somay.Substring(5, 1);
                    text7 = somay.Substring(6, 1);
                    text8 = somay.Substring(7, 1);
                    text9 = somay.Substring(8, 1);
                    break;
                case 10:
                    text = somay.Substring(1, 1);
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    text9 = somay.Substring(9, 1);
                    break;
            }
            string text10 = text + text2 + text3;
            string text11 = text4 + text5 + text6;
            string text12 = text7 + text8 + text9;
            if (text10 == text11 && text11 == text12)
            {
                return true;
            }
            return false;
        }

        public static bool ABCABC(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            if (text2 == text5 && text3 == text6 && text4 == text7 && !NamSinh(somay))
            {
                return true;
            }
            return false;
        }

        public static bool ABCDABCD(string somay)
        {
            string text = somay.Substring(0, 1);
            string text2 = somay.Substring(1, 1);
            string text3 = somay.Substring(2, 1);
            string text4 = somay.Substring(3, 1);
            string text5 = somay.Substring(4, 1);
            string text6 = somay.Substring(5, 1);
            string text7 = somay.Substring(6, 1);
            string text8 = somay.Substring(7, 1);
            string text9 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(1, 1);
                    text3 = somay.Substring(2, 1);
                    text4 = somay.Substring(3, 1);
                    text5 = somay.Substring(4, 1);
                    text6 = somay.Substring(5, 1);
                    text7 = somay.Substring(6, 1);
                    text8 = somay.Substring(7, 1);
                    text9 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(2, 1);
                    text3 = somay.Substring(3, 1);
                    text4 = somay.Substring(4, 1);
                    text5 = somay.Substring(5, 1);
                    text6 = somay.Substring(6, 1);
                    text7 = somay.Substring(7, 1);
                    text8 = somay.Substring(8, 1);
                    text9 = somay.Substring(9, 1);
                    break;
            }
            if (text2 + text3 + text4 + text5 == text6 + text7 + text8 + text9)
            {
                return true;
            }
            return false;
        }

        public static bool Taxi5(string somay)
        {
            string text = "";
            string text2 = somay.Substring(0, 1);
            string text3 = somay.Substring(1, 1);
            string text4 = somay.Substring(2, 1);
            string text5 = somay.Substring(3, 1);
            string text6 = somay.Substring(4, 1);
            string text7 = somay.Substring(5, 1);
            string text8 = somay.Substring(6, 1);
            string text9 = somay.Substring(7, 1);
            string text10 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(0, 1);
                    text3 = somay.Substring(1, 1);
                    text4 = somay.Substring(2, 1);
                    text5 = somay.Substring(3, 1);
                    text6 = somay.Substring(4, 1);
                    text7 = somay.Substring(5, 1);
                    text8 = somay.Substring(6, 1);
                    text9 = somay.Substring(7, 1);
                    text10 = somay.Substring(8, 1);
                    break;
                case 10:
                    text = somay.Substring(0, 1);
                    text2 = somay.Substring(1, 1);
                    text3 = somay.Substring(2, 1);
                    text4 = somay.Substring(3, 1);
                    text5 = somay.Substring(4, 1);
                    text6 = somay.Substring(5, 1);
                    text7 = somay.Substring(6, 1);
                    text8 = somay.Substring(7, 1);
                    text9 = somay.Substring(8, 1);
                    text10 = somay.Substring(9, 1);
                    break;
            }
            string text11 = text2 + text3 + text4 + text5;
            string text12 = text7 + text8 + text9 + text10;
            if (text11 == text12)
            {
                if (text == "")
                {
                    if (text6 == "0")
                    {
                        return true;
                    }
                }
                else if (text == text6)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool ThanTai_x79(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[25]
            {
            "000", "111", "222", "333", "444", "555", "666", "777", "888", "999",
            "668", "686", "688", "868", "886", "866", "234", "345", "456", "567",
            "678", "789", "779", "799", "979"
            };
            string[] source2 = new string[1] { "68" };
            string value2 = text5 + text6 + text7;
            string[] source3 = new string[7] { "179", "279", "379", "479", "579", "679", "879" };
            if (source3.Contains(value2) && !source2.Contains(text4 + text5) && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool ThanTai_079(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[25]
            {
            "000", "111", "222", "333", "444", "555", "666", "777", "888", "999",
            "668", "686", "688", "868", "886", "866", "234", "345", "456", "567",
            "678", "789", "779", "799", "979"
            };
            string[] source2 = new string[1] { "68" };
            string value2 = text5 + text6 + text7;
            string[] source3 = new string[1] { "079" };
            if (source3.Contains(value2) && !source2.Contains(text4 + text5) && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool ThanTai3(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[25]
            {
            "000", "111", "222", "333", "444", "555", "666", "777", "888", "999",
            "668", "686", "688", "868", "886", "866", "234", "345", "456", "567",
            "678", "789", "779", "799", "979"
            };
            string[] source2 = new string[1] { "7" };
            string value2 = text5 + text6 + text7;
            string[] source3 = new string[2] { "779", "979" };
            if (source3.Contains(value2) && !source2.Contains(text4) && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool ThanTai3Nut(string somay, string pattern)
        {
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool ThanTai3_779(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[25]
            {
            "000", "111", "222", "333", "444", "555", "666", "777", "888", "999",
            "668", "686", "688", "868", "886", "866", "234", "345", "456", "567",
            "678", "789", "779", "799", "979"
            };
            string[] source2 = new string[2] { "7", "9" };
            string text8 = text5 + text6 + text7;
            if (text8 == "779" && !source2.Contains(text4) && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool ThanTai3_979(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[25]
            {
            "000", "111", "222", "333", "444", "555", "666", "777", "888", "999",
            "668", "686", "688", "868", "886", "866", "234", "345", "456", "567",
            "678", "789", "779", "799", "979"
            };
            string[] source2 = new string[2] { "3", "7" };
            string text8 = text5 + text6 + text7;
            if (text8 == "979" && !source2.Contains(text4) && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool ThanTai3_799(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4;
            string[] source = new string[25]
            {
            "000", "111", "222", "333", "444", "555", "666", "777", "888", "999",
            "668", "686", "688", "868", "886", "866", "234", "345", "456", "567",
            "678", "789", "779", "799", "979"
            };
            string[] source2 = new string[1] { "7" };
            string text8 = text5 + text6 + text7;
            if (text8 == "799" && !source2.Contains(text4) && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool ThanTai4_7779(string somay)
        {
            string pattern = "([0-9]{5})(7779$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(7779$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(7779$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool ThanTai4_3339(string somay)
        {
            string pattern = "([0-9]{5})(3339$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(3339$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(3339$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool ThanTai4_6879(string somay)
        {
            string pattern = "([0-9]{5})(6879$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(6879$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(6879$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool ThanTai4_3979(string somay)
        {
            string pattern = "([0-9]{5})(3979$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(3979$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(3979$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool ThanTai4_3939(string somay)
        {
            string pattern = "([0-9]{5})(3939$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(3939$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(3939$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool ThanTai4_7979(string somay)
        {
            string pattern = "([0-9]{5})(7979$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(7979$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(7979$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool ThanTai4_8889(string somay)
        {
            string pattern = "([0-9]{5})(8889$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(8889$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(8889$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool ThanTai4_9998(string somay)
        {
            string pattern = "([0-9]{5})(9998$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(9998$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(9998$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool ThanTai4(string somay)
        {
            string pattern = "([0-9]{5})(6879$|3939$|3979$|7979$|7779$)";
            switch (somay.Length)
            {
                case 9:
                    pattern = "([0-9]{5})(6879$|3939$|3979$|7979$|7779$)";
                    break;
                case 10:
                    pattern = "([0-9]{6})(6879$|3939$|3979$|7979$|7779$)";
                    break;
            }
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool ThanTai4Nut(string somay, string pattern)
        {
            Regex regex = new Regex(pattern);
            Match match = regex.Match(somay);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public static bool TuQuy(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3;
            string[] source = new string[10] { "00", "11", "22", "33", "44", "55", "66", "77", "88", "99" };
            string value2 = text4 + text5 + text6 + text7;
            string[] source2 = new string[10] { "0000", "1111", "2222", "3333", "4444", "5555", "6666", "7777", "8888", "9999" };
            if (source2.Contains(value2) && !source.Contains(value) && text3 != text4)
            {
                return true;
            }
            return false;
        }

        public static bool TuQuy_0000(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3;
            string[] source = new string[10] { "00", "11", "22", "33", "44", "55", "66", "77", "88", "99" };
            string text8 = text4 + text5 + text6 + text7;
            if (text8 == "0000" && text3 != "0" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool TuQuy_1111(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3;
            string[] source = new string[10] { "00", "11", "22", "33", "44", "55", "66", "77", "88", "99" };
            string text8 = text4 + text5 + text6 + text7;
            if (text8 == "1111" && text3 != "1" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool TuQuy_2222(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3;
            string[] source = new string[10] { "00", "11", "22", "33", "44", "55", "66", "77", "88", "99" };
            string text8 = text4 + text5 + text6 + text7;
            if (text8 == "2222" && text3 != "2" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool TuQuy_3333(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3;
            string[] source = new string[10] { "00", "11", "22", "33", "44", "55", "66", "77", "88", "99" };
            string text8 = text4 + text5 + text6 + text7;
            if (text8 == "3333" && text3 != "3" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool TuQuy_4444(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3;
            string[] source = new string[10] { "00", "11", "22", "33", "44", "55", "66", "77", "88", "99" };
            string text8 = text4 + text5 + text6 + text7;
            if (text8 == "4444" && text3 != "4" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool TuQuy_5555(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3;
            string[] source = new string[10] { "00", "11", "22", "33", "44", "55", "66", "77", "88", "99" };
            string text8 = text4 + text5 + text6 + text7;
            if (text8 == "5555" && text3 != "5" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool TuQuy_6666(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3;
            string[] source = new string[10] { "00", "11", "22", "33", "44", "55", "66", "77", "88", "99" };
            string text8 = text4 + text5 + text6 + text7;
            if (text8 == "6666" && text3 != "6" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool TuQuy_7777(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3;
            string[] source = new string[10] { "00", "11", "22", "33", "44", "55", "66", "77", "88", "99" };
            string text8 = text4 + text5 + text6 + text7;
            if (text8 == "7777" && text3 != "7" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool TuQuy_8888(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3;
            string[] source = new string[10] { "00", "11", "22", "33", "44", "55", "66", "77", "88", "99" };
            string text8 = text4 + text5 + text6 + text7;
            if (text8 == "8888" && text3 != "8" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool TuQuy_9999(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3;
            string[] source = new string[10] { "00", "11", "22", "33", "44", "55", "66", "77", "88", "99" };
            string text8 = text4 + text5 + text6 + text7;
            if (text8 == "9999" && text3 != "9" && !source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool TuGiua(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4 + text5;
            string[] source = new string[10] { "0000", "1111", "2222", "3333", "4444", "5555", "6666", "7777", "8888", "9999" };
            if (source.Contains(value) && text6 != text5)
            {
                return true;
            }
            return false;
        }

        public static bool TuGiuaDau(string somay)
        {
            string text1 = somay.Substring(0, 1);
            string text2 = somay.Substring(1, 1);
            string text3 = somay.Substring(2, 1);
            string text4 = somay.Substring(3, 1);
            string text5 = somay.Substring(4, 1);
            string text6 = somay.Substring(5, 1);
            string text7 = somay.Substring(6, 1);
            string value = text3 + text4 + text5 + text6;
            string[] source = new string[10] { "0000", "1111", "2222", "3333", "4444", "5555", "6666", "7777", "8888", "9999" };
            if (source.Contains(value))
            {
                return true;
            }
            return false;
        }

        public static bool TuGiua_0000(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5;
            if (text8 == "0000" && text6 != "0")
            {
                return true;
            }
            return false;
        }

        public static bool TuGiua_1111(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5;
            if (text8 == "1111" && text6 != "1")
            {
                return true;
            }
            return false;
        }

        public static bool TuGiua_2222(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5;
            if (text8 == "2222" && text6 != "2")
            {
                return true;
            }
            return false;
        }

        public static bool TuGiua_3333(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5;
            if (text8 == "3333" && text6 != "3")
            {
                return true;
            }
            return false;
        }

        public static bool TuGiua_4444(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5;
            if (text8 == "4444" && text6 != "4")
            {
                return true;
            }
            return false;
        }

        public static bool TuGiua_5555(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5;
            if (text8 == "5555" && text6 != "5")
            {
                return true;
            }
            return false;
        }

        public static bool TuGiua_6666(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5;
            if (text8 == "6666" && text6 != "6")
            {
                return true;
            }
            return false;
        }

        public static bool TuGiua_7777(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5;
            if (text8 == "7777" && text6 != "7")
            {
                return true;
            }
            return false;
        }

        public static bool TuGiua_8888(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5;
            if (text8 == "8888" && text6 != "8")
            {
                return true;
            }
            return false;
        }

        public static bool TuGiua_9999(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string text8 = text2 + text3 + text4 + text5;
            if (text8 == "9999" && text6 != "9")
            {
                return true;
            }
            return false;
        }

        public static bool TuGiua_689(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4 + text5;
            string[] source = new string[3] { "6666", "8888", "9999" };
            if (source.Contains(value) && text6 != text5)
            {
                return true;
            }
            return false;
        }

        public static bool TuGiua_2357(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4 + text5;
            string[] source = new string[4] { "2222", "3333", "5555", "7777" };
            if (source.Contains(value) && text6 != text5)
            {
                return true;
            }
            return false;
        }

        public static bool TuGiua_014(string somay)
        {
            string text = somay.Substring(0, 3);
            string text2 = somay.Substring(3, 1);
            string text3 = somay.Substring(4, 1);
            string text4 = somay.Substring(5, 1);
            string text5 = somay.Substring(6, 1);
            string text6 = somay.Substring(7, 1);
            string text7 = somay.Substring(8, 1);
            switch (somay.Length)
            {
                case 9:
                    text2 = somay.Substring(3, 1);
                    text3 = somay.Substring(4, 1);
                    text4 = somay.Substring(5, 1);
                    text5 = somay.Substring(6, 1);
                    text6 = somay.Substring(7, 1);
                    text7 = somay.Substring(8, 1);
                    break;
                case 10:
                    text2 = somay.Substring(4, 1);
                    text3 = somay.Substring(5, 1);
                    text4 = somay.Substring(6, 1);
                    text5 = somay.Substring(7, 1);
                    text6 = somay.Substring(8, 1);
                    text7 = somay.Substring(9, 1);
                    break;
            }
            string value = text2 + text3 + text4 + text5;
            string[] source = new string[3] { "0000", "1111", "4444" };
            if (source.Contains(value) && text6 != text5)
            {
                return true;
            }
            return false;
        }
    }
}
