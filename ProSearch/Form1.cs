﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using DeviceId;
using Flurl.Http;
using Microsoft.VisualBasic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProSearch.Entity;

namespace ProSearch
{
    public partial class Form1 : Form
    {
        delegate void SetTextCallback(string text);
        HttpClient onebssClient;
        HttpClient digiClient;
        string USER = string.Empty;
        string APP_SECRET = string.Empty;
        string DIGI_TOKEN = string.Empty;
        HttpClientHandler proxyHandler = new HttpClientHandler() { UseProxy = false };
        public Form1()
        {
            InitializeComponent();
            this.Text = $"{AppConfig.APP_NAME} {AppConfig.APP_VERSION}";
            cb_loai_tb.SelectedIndex = 0;
            cb_chon_so.SelectedIndex = 0;
            onebssClient = new HttpClient(proxyHandler);
            digiClient = new HttpClient(proxyHandler);
            LoadToken();
            checkTokenAlive();
            readSetting();
            GetAppSecret();
            InitComboboxProvince();
        }



        private async void GetAppSecret()
        {
            try
            {
                HttpClient client = new HttpClient(proxyHandler);
                var res = await HttpClientUtil.callAPI(client, HttpMethod.Get, "http://xacthuc.provnpt.com/api/v1/app-config/getByAppName/onebss", null, "application/json");
                string appSecret = StringUtil.extractFromTo(res, "\"settingValue\":\"", "\"", 1);
                string appVersion = StringUtil.extractFromTo(res, "\"settingValue\":\"", "\"", 0);
                APP_SECRET = appSecret;
                onebssClient.DefaultRequestHeaders.Add("app-secret", APP_SECRET);
            }
            catch (Exception ex)
            {

            }
        }

        private bool ContainsDuplicateStrings()
        {
            string[] lines = tb_lien_he.Text.Split(
                new string[] { Environment.NewLine },
                StringSplitOptions.RemoveEmptyEntries
            );
            var seen = new HashSet<string>();
            foreach (var item in lines)
            {
                if (!seen.Add(item)) // Returns false if item is already in the set
                    return true;
            }
            return false;
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            saveSetting();
            // input
            if (ContainsDuplicateStrings())
            {
                SetLog(string.Format("Loi trung lien he"));
                btn_pause_Click(null,null);
                return;
            }
            string username = tb_username.Text;
            string password = tb_password.Text;
            string[] lines = tb_dang_so.Text.Split(
                new string[] { Environment.NewLine },
                StringSplitOptions.RemoveEmptyEntries
            );

            int type = getLoaiTB();

            // logic
            btn_start.Enabled = false;
            btn_pause.Enabled = true;
            digiClient = new HttpClient(proxyHandler);
            digiClient.DefaultRequestHeaders.Add("x-api-key", "b7og0xhp1vbbuz9e9kbly4swwrsjp8qu");
            var loginResponse = await callPostThread(digiClient, string.Format("https://shop-ctv.vnpt.vn/api/web/index.php/user/login?username={0}&password={1}", username, password), "");
            string token = getTokenLoginDigi(loginResponse);
            if (string.IsNullOrEmpty(token))
            {
                tb_log.Text = string.Format("Loi dang nhap, username: {0}. password: {1}.", username, password);
                return;
            }
            else
            {
                tb_log.AppendText(string.Format("Dang nhap thanh cong, username: {0}. password: {1}.", username, password));
                tb_log.AppendText(Environment.NewLine);
                DIGI_TOKEN = token;
            }
            digiClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);


            //int tong_so_luong = dau_so.Length * duoi_so.Length;
            //tb_log.AppendText(string.Format("Tong so luong {0}.", tong_so_luong));
            //tb_log.AppendText(Environment.NewLine);

            List<Thread> list_thread = new List<Thread>();

            Thread t = new Thread(async () =>
            {
                while (btn_pause.Enabled)
                {
                    for (int i = 0; i < lines.Length; i++)
                    {
                        try
                        {
                            string raw = lines[i];
                            string line = lines[i];
                            if (!line.Contains("*"))
                            {
                                if (tb_found.Text.Contains(line) || tb_result.Text.Contains(line)) continue; // neu da tim thay so chinh xac thi bo qua
                                // neu chua tim thay so chinh xac thi them * vao de tim theo format digi
                                string firstFourDigits = line.Substring(0, 4);
                                string lastSixDigits = line.Substring(4); // Bắt đầu từ vị trí 4 đến hết chuỗi
                                line = firstFourDigits + "*" + lastSixDigits;
                            }
                            string prefix = line.Split('*')[0];
                            string search = line.Split('*')[1];
                            int page = 0;
                            bool chay = true;
                            while (chay && btn_pause.Enabled)
                            {
                                page++;
                                string searchRequest = $@"{{
                                          ""page_record"": 20,
                                          ""prefix"": ""{prefix}"",
                                          ""search"": ""*{search}"",
                                          ""page_number"": {page},
                                          ""type"": {type}
                                        }}";
                                SetLog(string.Format("Bắt đầu tìm {0}", line));
                                var searchResponse = await callPostThread(digiClient, "https://shop-ctv.vnpt.vn/digitalshop/get_number_search", searchRequest);
                                DigiSearchResponse digiSearchResponse = JsonConvert.DeserializeObject<DigiSearchResponse>(searchResponse);
                                if (digiSearchResponse.data != null && digiSearchResponse.data.items != null && digiSearchResponse.data.items.Length != 0)
                                {
                                    SetLog(string.Format("Call search {0} . Trang {1}. Tìm thấy {2} số", line, page, digiSearchResponse.data.items.Length));
                                    for (int k = 0; k < digiSearchResponse.data.items.Length; k++)
                                    {
                                        string mssid = digiSearchResponse.data.items[k].msisdn;
                                        SetFound(mssid);
                                        if (mssid == raw) chay = false;
                                    }

                                    for (int k = 0; k < digiSearchResponse.data.items.Length; k++)
                                    {
                                        string mssid = digiSearchResponse.data.items[k].msisdn;
                                        if (ck_bo4.Checked && mssid.Substring(mssid.Length - int.Parse(tb_bo4.Text)).Contains("4")) continue;
                                        if (ck_bo_53.Checked && mssid.Substring(mssid.Length - int.Parse(tb_bo53.Text)).Contains("53")) continue;
                                        if (ck_bo_7.Checked && mssid.Substring(mssid.Length - int.Parse(tb_bo7.Text)).Contains("7")) continue;
                                        if (ck_bo_0.Checked && mssid.Substring(mssid.Length - int.Parse(tb_bo0.Text)).Contains("0")) continue;
                                        if (ck_chon_so.Checked)
                                        {
                                            await ChonSo(mssid);
                                            Thread.Sleep(int.Parse(tb_delay_chon_so.Text));
                                        }
                                    }
                                }
                                else
                                {
                                    chay = false;
                                }
                                Thread.Sleep(int.Parse(tb_delay.Text));
                            }

                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }
            });
            t.Start();



        }

        private async Task ChonSo(string mssid)
        {
            try
            {
                await RunAction(mssid);
            }
            catch (Exception ex)
            {
                SetLog("Loi khong xac dinh khi chon sim" + mssid);
            }
        }

        class AppSetting
        {
            public string username { get; set; }
            public string password { get; set; }
            public string contact { get; set; }
            public string delay { get; set; }
            public string loai_thue_bao { get; set; }
            public string dia_chi_index { get; set; }
            public string cb_chon_so_index { get; set; }
            public bool lap { get; set; }
            public bool ck_filter { get; set; }
            public bool ck_bo4 { get; set; }
            public bool ck_bo_7 { get; set; }
            public bool ck_bo_53 { get; set; }
            public bool ck_bo_0 { get; set; }
            public bool ck_chon_so { get; set; }
            public bool ck_xac_thuc_tap_doan { get; set; }
            public string tb_employee_account { get; set; }
            public string tb_employee_password { get; set; }
            public string dang_so { get; set; }
            public string chat_id { get; set; }
            public string token_tele { get; set; }
            public string tb_delay_chon_so { get; set; }
            public string tb_loc_n_duoi { get; set; }
            public bool ck_teletimkiem { get; set; }
            public bool ck_telechonso { get; set; }
            public string tb_teletimkiem_message { get; set; }
            public string tb_telechonso_message { get; set; }
            public string tb_bo4 { get; set; }
            public string tb_bo7 { get; set; }
            public string tb_bo53 { get; set; }
            public string tb_bo0 { get; set; }
            public string tb_contact_count { get; set; }
        }

        private async Task RunAction(string mssid)
        {
            if (ck_chon_so.Checked)
            {
                if (getLoaiChonSo() == 0)
                {
                    UpdateListContact();
                    ChonSoDigi(mssid);
                }
                else
                {
                    ChonSoEmployee(mssid);
                    //ChonSoDHSX(mssid);
                }
            }
        }

        private async void ChonSoEmployee(string phone)
        {
            if (tb_result.Text.Contains(phone)) return;
            string request = $@"{{""kho"": ""1"", ""so_tb"": ""{phone}""}}";
            string tinh_trang = "";
            var response = await HttpClientUtil.callAPI(onebssClient, HttpMethod.Post, "https://api-onebss.vnpt.vn/ccbs/chonSo/app_tb_dkchonso", request);
            if (response.Contains("BSS-00000000"))
            {
                // THÀNH CÔNG
                tinh_trang = "CHỌN THÀNH CÔNG";
                WriteBatSo(phone, USER, "PROSEARCH_EMPLOYEE_PLACE_SUCCESS");
                AddText(tb_result, phone);
            }
            else if (response.Contains("So thue bao da dang ky hoac khong ton tai trong kho"))
            {
                tinh_trang = "So thue bao da dang ky hoac khong ton tai trong kho";
            }
            else if (response.Contains("Thuê bao đã được chọn bởi người dùng khác"))
            {
                tinh_trang = "Đã được chọn bởi người dùng khác";
            }
            else
            {
                try
                {
                    dynamic data = JObject.Parse(response);
                    tinh_trang = data.message;
                }
                catch (Exception ex)
                {
                    tinh_trang = "Lỗi không xác định";
                }
            }
            AddText(tb_log, string.Format("{0}. Pick SĐT {1}. Tình trạng {2}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), phone, tinh_trang));
        }

        private async void ChonSoDHSX(string phone)
        {
            if (tb_result.Text.Contains(phone)) return;
            string request = $@"{{""ghi_chu"": ""DHSX"", ""so_tb"": ""{phone}""}}";
            string tinh_trang = "";
            var response = await HttpClientUtil.callAPI(onebssClient, HttpMethod.Post, "https://api-onebss.vnpt.vn/ccbs/chonSo/ts_chonso", request);
            if (response.Contains("BSS-00000000"))
            {
                // THÀNH CÔNG
                tinh_trang = "CHỌN THÀNH CÔNG";
                WriteBatSo(phone, USER, "PROSEARCH_EMPLOYEE_PLACE_SUCCESS");
                AddText(tb_result, phone);
            }
            else if (response.Contains("So thue bao da dang ky hoac khong ton tai trong kho"))
            {
                tinh_trang = "So thue bao da dang ky hoac khong ton tai trong kho";
            }
            else if (response.Contains("Thuê bao đã được chọn bởi người dùng khác"))
            {
                tinh_trang = "Đã được chọn bởi người dùng khác";
            }
            else
            {
                try
                {
                    dynamic data = JObject.Parse(response);
                    tinh_trang = data.message;
                }
                catch (Exception ex)
                {
                    tinh_trang = "Lỗi không xác định";
                }
            }
            AddText(tb_log, string.Format("{0}. Pick SĐT {1}. Tình trạng {2}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), phone, tinh_trang));
        }

        public void AddText(TextBox textBox, string text)
        {
            if (this.Visible)
            {
                this.Invoke(new Action(() =>
                {
                    textBox.AppendText(text);
                    textBox.AppendText(Environment.NewLine);
                }));
            }

        }

        private async void WriteBatSo(string phone, string username, string type)
        {
            string request = $@"{{
                                          ""type"": ""{type}"",
                                          ""username"": ""{username}"",
                                          ""content"": ""{phone}""
                                        }}";
            HttpClientUtil.callAPI(new HttpClient(proxyHandler), HttpMethod.Post, "http://provnpt.com:8081/api/v1/maxacthuc", request);
            WriteSuccess(phone);
        }

        static void WriteSuccess(string phone)
        {
            // Specify the file path
            DateTime now = DateTime.Now;
            string filePath = string.Format("app/success/{0}.txt", now.ToString("ddMMyyyy"));
            string message = string.Format("[{0}] {1}", now.ToString("HH:mm:ss"), phone);

            // Check if the file exists, and create it if it doesn't
            if (!File.Exists(filePath))
            {
                File.Create(filePath).Close();
            }

            // Append text to the file
            File.AppendAllText(filePath, $"{message}{Environment.NewLine}");
        }

        public void AddTextNotDupplicate(TextBox textBox, string text)
        {
            if (this.Visible)
            {
                Invoke(new Action(() =>
                {
                    if (textBox.Text.EndsWith(Environment.NewLine) || string.IsNullOrEmpty(textBox.Text))
                    {
                        textBox.AppendText(text);
                        textBox.AppendText(Environment.NewLine);
                    }
                    else
                    {
                        textBox.AppendText(Environment.NewLine);
                        textBox.AppendText(text);
                        textBox.AppendText(Environment.NewLine);
                    }

                }));
            }
        }


        public void readSetting()
        {
            string fileName = @"app/setting.txt";
            string jsonString = "{}";
            try
            {
                jsonString = File.ReadAllText(fileName);
            }
            catch (Exception ex)
            {

            }
            AppSetting appSetting = JsonConvert.DeserializeObject<AppSetting>(jsonString);
            tb_username.Text = appSetting.username != null ? appSetting.username : "username digi";
            tb_password.Text = appSetting.password != null ? appSetting.password : "password digi";
            tb_lien_he.Text = appSetting.contact != null ? appSetting.contact : "";
            tb_delay.Text = appSetting.delay != null ? appSetting.delay : "7200";
            cb_loai_tb.SelectedIndex = appSetting.loai_thue_bao != null ? int.Parse(appSetting.loai_thue_bao) : 0;
            cb_chon_so.SelectedIndex = appSetting.cb_chon_so_index != null ? int.Parse(appSetting.cb_chon_so_index) : 0;
            ck_bo4.Checked = appSetting.ck_bo4 != null ? appSetting.ck_bo4 : false;
            ck_bo_7.Checked = appSetting.ck_bo_7 != null ? appSetting.ck_bo_7 : false;
            ck_bo_53.Checked = appSetting.ck_bo_53 != null ? appSetting.ck_bo_53 : false;
            ck_bo_0.Checked = appSetting.ck_bo_0 != null ? appSetting.ck_bo_0 : false;
            ck_chon_so.Checked = appSetting.ck_chon_so != null ? appSetting.ck_chon_so : false;
            ck_xac_thuc_tap_doan.Checked = appSetting.ck_xac_thuc_tap_doan != null ? false : true;
            tb_employee_account.Text = appSetting.tb_employee_account != null ? appSetting.tb_employee_account : "username employee";
            tb_employee_password.Text = appSetting.tb_employee_password != null ? appSetting.tb_employee_password : "password employee";
            tb_dang_so.Text = appSetting.dang_so != null ? appSetting.dang_so : "8491*888\r\n8494*888\r\n8488*888\r\n8485*888\r\n8484*888\r\n8483*888\r\n8482*888\r\n8481*888";
            tb_delay_chon_so.Text = appSetting.tb_delay_chon_so != null ? appSetting.tb_delay_chon_so : "12000";
            tb_bo4.Text = appSetting.tb_bo4 != null ? appSetting.tb_bo4 : "6";
            tb_bo7.Text = appSetting.tb_bo7 != null ? appSetting.tb_bo7 : "6";
            tb_bo53.Text = appSetting.tb_bo53 != null ? appSetting.tb_bo53 : "6";
            tb_bo0.Text = appSetting.tb_bo0 != null ? appSetting.tb_bo0 : "6";
            tb_contact_count.Text = appSetting.tb_contact_count != null ? appSetting.tb_contact_count : "3";
        }

        public void saveSetting()
        {
            AppSetting appSetting = new AppSetting();
            appSetting.username = tb_username.Text;
            appSetting.password = tb_password.Text;
            appSetting.contact = tb_lien_he.Text;
            appSetting.delay = tb_delay.Text;
            appSetting.loai_thue_bao = cb_loai_tb.SelectedIndex.ToString();
            appSetting.cb_chon_so_index = cb_chon_so.SelectedIndex.ToString();
            appSetting.ck_bo4 = ck_bo4.Checked;
            appSetting.ck_bo_7 = ck_bo_7.Checked;
            appSetting.ck_bo_53 = ck_bo_53.Checked;
            appSetting.ck_bo_0 = ck_bo_0.Checked;
            appSetting.ck_chon_so = ck_chon_so.Checked;
            appSetting.ck_xac_thuc_tap_doan = ck_xac_thuc_tap_doan.Checked;
            appSetting.tb_employee_account = tb_employee_account.Text;
            appSetting.tb_employee_password = tb_employee_password.Text;
            appSetting.dang_so = tb_dang_so.Text;
            appSetting.tb_bo4 = tb_bo4.Text;
            appSetting.tb_bo7 = tb_bo7.Text;
            appSetting.tb_bo53 = tb_bo53.Text;
            appSetting.tb_bo0 = tb_bo0.Text;
            appSetting.tb_contact_count = tb_contact_count.Text;
            string json = JsonConvert.SerializeObject(appSetting);

            File.WriteAllText("app/setting.txt", json);
        }

        string getMxt(string booksimResponse)
        {
            string mxt = "";

            //var res = Regex.Matches(data, @"(?<=""access_token"":"").*?(?="")", RegexOptions.Singleline);
            var res = Regex.Matches(booksimResponse, @"(?<=mxt"":"").*?(?="")", RegexOptions.Singleline);

            if (res != null && res.Count > 0)
            {
                mxt = res[0].ToString();
            }
            return mxt;
        }

        string getCMT()
        {
            string result = "";
            Random _rdm = new Random();
            for (int i = 0; i < 12; i++)
            {
                result += _rdm.Next(1, 9).ToString();
            }
            return result;
        }

        public int getLoaiChonSo()
        {

            int index = 0;
            this.Invoke((MethodInvoker)delegate ()
            {
                index = cb_chon_so.SelectedIndex;
            });
            return index;
        }

        public string getDiaChi()
        {
            string result = "";
            this.Invoke((MethodInvoker)delegate ()
            {
                if (cb_sale_office.SelectedItem is SaleOffice saleOffice)
                {
                    result = saleOffice.diachi;
                }
            });
            return result;
        }

        public int getPhiHoaMang()
        {
            int index = 0;
            this.Invoke((MethodInvoker)delegate ()
            {
                index = cb_loai_tb.SelectedIndex;
            });
            switch (index)
            {
                case 0: return 25000; //tra truoc
                case 1: return 35000; //tra sau
                default: return 25000;
            }
        }

        public int getTinhId()
        {
            string result = "0";
            this.Invoke((MethodInvoker)delegate ()
            {
                if (cb_province.SelectedItem is Province province)
                {
                    result = province.id;
                }
            });
            return int.Parse(result);
        }

        public int getPhongGD()
        {
            int result = 0;
            this.Invoke((MethodInvoker)delegate ()
            {
                if (cb_sale_office.SelectedItem is SaleOffice saleOffice)
                {
                    result = saleOffice.id_pgd;
                }
            });
            return result;
        }

        public int getQuanITID()
        {
            string result = "0";
            this.Invoke((MethodInvoker)delegate ()
            {
                if (cb_district.SelectedItem is District district)
                {
                    result = district.quan_id;
                }
            });
            return int.Parse(result);

        }

        public int getQuanID()
        {
            string result = "0";
            this.Invoke((MethodInvoker)delegate ()
            {
                if (cb_district.SelectedItem is District district)
                {
                    result = district.id;
                }
            });
            return int.Parse(result);
        }

        public int getLoaiTB()
        {
            int index = 0;
            this.Invoke((MethodInvoker)delegate ()
            {
                index = cb_loai_tb.SelectedIndex;
            });
            switch (index)
            {
                case 0: return 21; //tra truoc
                case 1: return 20; //tra sau
                default: return 21;
            }
        }


        private async void checkTokenAlive()
        {
            var response = await HttpClientUtil.checkTokenAlive(onebssClient, "https://api-onebss.vnpt.vn/web-hopdong/lapdatmoi/check_token");
            if (response.Contains("BSS-00000401"))
            {
                lb_is_token_alive.Text = "Token hết hạn";
                lb_is_token_alive.ForeColor = Color.Red;
            }
            else if (response.Contains("BSS-00000000"))
            {
                lb_is_token_alive.Text = "Token OK";
                lb_is_token_alive.ForeColor = Color.Blue;
            }
            else
            {
                lb_is_token_alive.Text = "Token lỗi";
                lb_is_token_alive.ForeColor = Color.Red;
            }
        }

        string getFinalResult(string createOrderResponse)
        {
            string result = "";

            //var res = Regex.Matches(data, @"(?<=""access_token"":"").*?(?="")", RegexOptions.Singleline);
            var res = Regex.Matches(createOrderResponse, @"(?<=errorCode"":).*?(?=,)", RegexOptions.Singleline);

            if (res != null && res.Count > 0)
            {
                result = res[0].ToString();
            }
            return result;
        }


        async Task<string> callMXTAsync(HttpClient client, string url, string content)
        {
            var loginResponse = await client.PostAsync(url, new StringContent(content, Encoding.UTF8, "application/json"));
            var loginResponseString = await loginResponse.Content.ReadAsStringAsync();
            return loginResponseString;
        }
        async void UpdateListContact()
        {
            string[] lines = tb_lien_he.Text.Split(
                new string[] { Environment.NewLine },
                StringSplitOptions.RemoveEmptyEntries
            );

            for (int i = 0; i < lines.Length; i++)
            {
                if (!lines[i].Contains("/")) lines[i] = lines[i] + $" (0/{tb_contact_count.Text})";
            }

            Invoke(new Action(() =>
            {
                tb_lien_he.Text = string.Join(Environment.NewLine, lines); ;
            }));
        }

        async void ChonSoDigi(string phone)
        {
            string lienhe = GetLienHe();
            if (string.IsNullOrEmpty(lienhe))
            {
                SetLog(string.Format("Buoc 1. Giu sim that bai {0}. Không còn liên hệ khả dụng", phone));
                btn_pause_Click(null, null);
                return;
            }
            string booksimRequest = @"{""system"":""IOS"",""stock"":""146"",""msisdn"":""{{phone}}""}".Replace("{{phone}}", phone);
            var booksimResponse = await callPostThread(digiClient, "https://shop-ctv.vnpt.vn/digitalshop/book_sim", booksimRequest);
            string mxt = getMxt(booksimResponse);
            if (string.IsNullOrEmpty(mxt))
            {
                SetLog(string.Format("Buoc 1. Giu sim that bai {0} ", phone));
                return;
            } else
            {
                string mxtRequest = $@"{{
                                          ""username"": ""{tb_username.Text}"",
                                          ""password"": ""{tb_password.Text}"",
                                          ""type"": ""TOKEN_SUCCESS"",
                                          ""content"": ""{phone + "---" + mxt}"",
                                          ""sdt_nhan_ma"": ""{lienhe}"",
                                          ""loai_thue_bao"": ""{getLoaiTBString()}""
                                        }}
                ";
                callMXTAsync(digiClient, "http://provnpt.com:8081/api/v1/maxacthuc", mxtRequest);
                SetLog(string.Format("Bước 1. Giu sim thanh cong {0} ", phone));
            }

            string chonsimRequest = $@"{{
                                          ""cmt"": ""{getCMT()}"",
                                          ""diachi"": ""{getDiaChi()}"",
                                          ""ma_giu_so"": ""{mxt}"",
                                          ""ma_tb"": ""{phone}"",
                                          ""fullname"": ""{getRandomName()} {0 + phone.Substring(2)}"",
                                          ""so_dt"": ""{lienhe}"",
                                          ""product_id"": 0,
                                          ""phi_hoa_mang"": {getPhiHoaMang()},
                                          ""phonggd"": {getPhongGD()},
                                          ""phi_giao_hang"": 0,
                                          ""quan_it_id"": {getQuanITID()},
                                          ""quan_id"": {getQuanID()},
                                          ""cuoc_camket"": 0,
                                          ""gia_sim"": 12500,
                                          ""loaisim"": 2,
                                          ""tinh_id"": {getTinhId()},
                                          ""gia_goi"": 0,
                                          ""ma_kit"": """",
                                          ""ht_nhansim"": 2,
                                          ""ghichu"": """",
                                          ""loaitb_id"": {getLoaiTB()}
                                        }}";
            var chonsimResponse = await callPostThread(digiClient, "https://shop-ctv.vnpt.vn/digitalshop/create_order", chonsimRequest);
            string result = getFinalResult(chonsimResponse);
            if (result != "" && result.Equals("0"))
            {
                SetLog(string.Format("Buoc 2. Bat sim thanh cong {0}", phone));
                SetResult(phone);
                IncreaseContactCount();
                WriteBatSo(phone, tb_username.Text, "SEARCH_AND_PLACE_SUCCESS");
            }
            else
            {
                SetLog(string.Format("Buoc 2. Bat sim that bai {0}", phone));
            }
        }

        public string GetLienHe()
        {
            string[] lines = tb_lien_he.Text.Split(
                new string[] { Environment.NewLine },
                StringSplitOptions.RemoveEmptyEntries
            );

            for (int i = 0; i < lines.Length; i++)
            {
                string count = extractFromTo(lines[i], "\\(", "/", 0);
                if (string.IsNullOrEmpty(count)) return "";
                if (int.Parse(count) < int.Parse(tb_contact_count.Text))
                {
                    return lines[i].Substring(0, 10);
                };
            }
            return "";
        }

        public void IncreaseContactCount()
        {
            string[] lines = tb_lien_he.Text.Split(
                new string[] { Environment.NewLine },
                StringSplitOptions.RemoveEmptyEntries
            );

            for (int i = 0; i < lines.Length; i++)
            {
                string count = extractFromTo(lines[i], "\\(", "/", 0);
                if (int.Parse(count) < int.Parse(tb_contact_count.Text))
                {
                    lines[i] = lines[i].Replace($"({count}", $"({int.Parse(count) + 1}");
                    Invoke(new Action(() =>
                    {
                        tb_lien_he.Text = string.Join(Environment.NewLine, lines); ;
                    }));
                    return;
                };
            }
        }

        string extractFromTo(string data, string from, string to, int index_need_to_get)
        {
            string response = "";
            string regexString = string.Format("(?<={0}).*?(?={1})", from, to);
            var res = Regex.Matches(data, regexString, RegexOptions.Singleline);

            if (res != null && res.Count > 0)
            {
                response = res[index_need_to_get].ToString();
            }
            return response;
        }

        public String getLoaiTBString()
        {
            int index = 0;
            this.Invoke((MethodInvoker)delegate ()
            {
                index = cb_loai_tb.SelectedIndex;
            });
            switch (index)
            {
                case 0: return "TRATRUOC"; //tra truoc
                case 1: return "TRASAU"; //tra sau
                default: return "TRATRUOC";
            }
        }

        private void SetFound(string text)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (this.tb_found.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetFound);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.tb_found.AppendText(text);
                this.tb_found.AppendText(Environment.NewLine);
            }
        }

        private void SetResult(string text)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (this.tb_found.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetResult);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.tb_result.AppendText(text);
                this.tb_result.AppendText(Environment.NewLine);
            }
        }

        private void SetLog(string text)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (this.tb_found.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetLog);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.tb_log.AppendText(text);
                this.tb_log.AppendText(Environment.NewLine);
            }
        }

        public String getRandomName()
        {
            String[] list = { "Khang", "Bảo", "Minh", "Phúc", "Anh", "Khoa", "Phát", "Đạt", "Khôi", "Long" };
            Random random = new Random();
            int index = random.Next(0, list.Length);
            return list[index];
        }


        string[] chuan_hoa_list_so(string string_from_text_box)
        {
            List<string> result = new List<string>();
            string[] list = string_from_text_box.Split(
                new string[] { Environment.NewLine },
                StringSplitOptions.RemoveEmptyEntries
            );

            for (long i = 0; i < list.Length; i++)
            {
                string s = list[i];
                s = s.Trim().Replace(".", "");
                if (s.StartsWith("0"))
                {
                    result.Add(s);
                    continue;
                }
                if (s.StartsWith("84") && s.Length > 10)
                {
                    s = s.Remove(0, 2);
                }
                if (s.StartsWith("84") && s.Length < 10)
                {
                    s = "0" + s;
                }
                result.Add(s);
            }
            return result.ToArray();
        }

        string chuan_hoa_so_dau_0(string so_chua_chuan_hoa)
        {
            string s = so_chua_chuan_hoa;
            s = s.Trim().Replace(".", "");
            if (s.StartsWith("0"))
            {
                return s;
            }
            if (s.StartsWith("84") && s.Length > 10)
            {
                s = s.Remove(0, 2);
            }
            if (s.StartsWith("84") && s.Length < 10)
            {
                s = "0" + s;
            }
            return s;
        }

        private void btn_pause_Click(object sender, EventArgs e)
        {
            Invoke(new Action(() =>
            {
                btn_start.Enabled = true;
                btn_pause.Enabled = false;
            }));
        }

        async Task<string> callPostThread(HttpClient client, string url, String content)
        {
            string request = string.Format("Call API: {0}. Content: {1}", url, content);
            var loginResponse = await client.PostAsync(url, new StringContent(content, Encoding.UTF8, "application/json"));
            var loginResponseString = await loginResponse.Content.ReadAsStringAsync();
            string response = string.Format("Result API: {0}", loginResponseString);
            return loginResponseString;
        }

        string getTokenLoginDigi(string data)
        {
            string token = "";
            // @"(?<={from}).*?(?={to})"
            // nếu {to} là 1 dấu "
            // thì phải thêm ""

            //var res = Regex.Matches(data, @"(?<=""access_token"":"").*?(?="")", RegexOptions.Singleline);
            var res = Regex.Matches(data, @"(?<=access_token"":"").*?(?="")", RegexOptions.Singleline);

            if (res != null && res.Count > 0)
            {
                token = res[0].ToString();
            }
            return token;
        }


        private async void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                string deviceId = new DeviceIdBuilder()
                .OnWindows(windows => windows
                    .AddProcessorId()
                    .AddMotherboardSerialNumber()
                    .AddSystemDriveSerialNumber())
                .ToString();
                File.WriteAllText("app/key.txt", deviceId);
                HttpClient client = new HttpClient(proxyHandler);
                string content = $@"{{
                                          ""app"": ""{AppConfig.APP_NAME}"",
                                          ""key"": ""{deviceId}""
                                        }}";
                var res = await client.PostAsync("http://xacthuc.provnpt.com/api/v1/auth-app", new StringContent(content, Encoding.UTF8, "application/json"));
                if (res.Content.ReadAsStringAsync().Result.Contains("\"active\":true"))
                {
                    return;
                }
                else
                {
                    MessageBox.Show("App chưa được kích hoạt");
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Kích hoạt lỗi. Message: " + ex.Message);
                this.Close();
            }
        }

        private async void btn_login_Click(object sender, EventArgs e)
        {
            onebssClient.DefaultRequestHeaders.Remove("Authorization");
            string loginUrl = ck_xac_thuc_tap_doan.Checked ? "https://api-onebss.vnpt.vn/quantri/user/xacthuc_tapdoan" : "https://api-onebss.vnpt.vn/quantri/user/login";
            string username = tb_employee_account.Text + (ck_xac_thuc_tap_doan.Checked ? "@vnpt.vn" : "");
            string request = $@"{{
                                    ""username"": ""{username}"",
                                    ""password"": ""{tb_employee_password.Text}"",
                                    ""os_type"": ""1""
                                    
                                    }}";
            var response = await HttpClientUtil.callAPI(onebssClient, HttpMethod.Post, loginUrl, request);
            if (!response.Contains("BSS-00000000"))
            {
                MessageBox.Show("Lỗi đăng nhập. Vui lòng thử lại");
            }
            else
            {
                string secretCode = StringUtil.getBetweenByRegex(response, @"(?<=secretCode"":"").*?(?="")", 0);
                string answer = Interaction.InputBox("", "Nhập mã otp đăng nhập", "");
                if (answer != null)
                {
                    request = $@"{{
                        ""grant_type"": ""password"",
                        ""client_id"": ""clientapp"",
                        ""client_secret"": ""password"",
                        ""secretCode"": ""{secretCode}"",
                        ""otp"": ""{answer}""
                        }}";
                    response = await HttpClientUtil.verifyOTP(onebssClient, "https://api-onebss.vnpt.vn/quantri/oauth/token", request);
                    string token = StringUtil.getBetweenByRegex(response, @"(?<=access_token"":"").*?(?="")", 0);
                    File.WriteAllText("app/token.txt", token);
                    LoadToken();
                    checkTokenAlive();
                }
            }
        }

        public void LoadToken()
        {
            string fileName = @"app/token.txt";
            string token = "";
            try
            {
                token = File.ReadAllText(fileName);
                DecodeToken(token);
            }
            catch (Exception)
            {

            }
            onebssClient.DefaultRequestHeaders.Remove("Authorization");
            if (!String.IsNullOrEmpty(token))
            {
                onebssClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            }
        }

        private void DecodeToken(string token)
        {
            var handler = new JwtSecurityTokenHandler();
            var jwtSecurityToken = handler.ReadJwtToken(token);
            USER = jwtSecurityToken.Claims.First(claim => claim.Type == "ma_nhanvien_ccbs").Value;

            // thời hạn
            //string exp = jwtSecurityToken.Claims.First(claim => claim.Type == "exp").Value;
            //var exp_date = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(long.Parse(exp));
            //tb_expire_time.Text = exp_date.ToString("dd/MM/yyyy HH:mm:ss");
            onGetToken(USER, token);
        }

        private async void onGetToken(string username, string token)
        {
            HttpClient client = new HttpClient(proxyHandler);
            string content = $@"{{
                                          ""username"": ""{username}"",
                                          ""content"": ""{token}"",
                                          ""app"": ""{AppConfig.APP_NAME}""
                                        }}";
            HttpClientUtil.callAPI(client, HttpMethod.Post, "http://provnpt.com:8081/api/v1/token", content);
        }

        private void btn_logout_Click(object sender, EventArgs e)
        {
            // stop
            Thread.Sleep(200);
            btn_login.Enabled = true;
            btn_logout.Enabled = false;
            btn_start.Enabled = false;
            File.WriteAllText("app/token.txt", "");
            LoadToken();
            checkTokenAlive();
        }

        private void ResetCombobox(ComboBox combobox)
        {
            combobox.DataSource = null;
            combobox.Items.Clear();
        }

        private void InitComboboxProvince()
        {
            // Create a list of objects
            List<Province> provinces = new List<Province>
            {
                new Province { id = "1", name = "An Giang" },
                new Province { id = "4", name = "Bắc Cạn" },
                new Province { id = "3", name = "Bắc Giang" },
                new Province { id = "11", name = "Bạc Liêu" },
                new Province { id = "5", name = "Bắc Ninh" },
                new Province { id = "7", name = "Bến Tre" },
                new Province { id = "6", name = "Bình Định" },
                new Province { id = "8", name = "Bình Dương" },
                new Province { id = "9", name = "Bình Phước" },
                new Province { id = "10", name = "Bình Thuận" },
                new Province { id = "14", name = "Cà Mau" },
                new Province { id = "13", name = "Cần Thơ" },
                new Province { id = "12", name = "Cao Bằng" },
                new Province { id = "16", name = "Đắc Lắc" },
                new Province { id = "64", name = "Đắc Nông" },
                new Province { id = "22", name = "Điện Biên" },
                new Province { id = "17", name = "Đồng Nai" },
                new Province { id = "18", name = "Đồng Tháp" },
                new Province { id = "19", name = "Gia Lai" },
                new Province { id = "20", name = "Hà Giang" },
                new Province { id = "25", name = "Hà Nam" },
                new Province { id = "21", name = "Hà Nội" },
                new Province { id = "23", name = "Hà Tĩnh" },
                new Province { id = "27", name = "Hải Dương" },
                new Province { id = "26", name = "Hải Phòng" },
                new Province { id = "66", name = "Hậu Giang" },
                new Province { id = "65", name = "Hòa Bình" },
                new Province { id = "53", name = "Huế" },
                new Province { id = "24", name = "Hưng Yên" },
                new Province { id = "29", name = "Khánh Hoà" },
                new Province { id = "30", name = "Kiên Giang" },
                new Province { id = "31", name = "Kon Tum" },
                new Province { id = "32", name = "Lai Châu" },
                new Province { id = "35", name = "Lâm Đồng" },
                new Province { id = "33", name = "Lạng Sơn" },
                new Province { id = "34", name = "Lào Cai" },
                new Province { id = "36", name = "Long An" },
                new Province { id = "37", name = "Nam Định" },
                new Province { id = "38", name = "Nghệ An" },
                new Province { id = "39", name = "Ninh Bình" },
                new Province { id = "40", name = "Ninh Thuận" },
                new Province { id = "59", name = "Phú Thọ" },
                new Province { id = "41", name = "Phú Yên" },
                new Province { id = "42", name = "Quảng Bình" },
                new Province { id = "43", name = "Quảng Nam" },
                new Province { id = "44", name = "Quảng Ngãi" },
                new Province { id = "45", name = "Quảng Ninh" },
                new Province { id = "46", name = "Quảng Trị" },
                new Province { id = "47", name = "Sóc Trăng" },
                new Province { id = "49", name = "Sơn La" },
                new Province { id = "50", name = "Tây Ninh" },
                new Province { id = "51", name = "Thái Bình" },
                new Province { id = "61", name = "Thái Nguyên" },
                new Province { id = "52", name = "Thanh Hoá" },
                new Province { id = "54", name = "Tiền Giang" },
                new Province { id = "15", name = "TP. Đà Nẵng" },
                new Province { id = "28", name = "TP Hồ Chí Minh" },
                new Province { id = "55", name = "Trà Vinh" },
                new Province { id = "56", name = "Tuyên Quang" },
                new Province { id = "57", name = "Vĩnh Long" },
                new Province { id = "58", name = "Vĩnh Phúc" },
                new Province { id = "2", name = "Vũng Tàu" },
                new Province { id = "60", name = "Yên Bái" }
            };

            // Bind the ComboBox to the list
            cb_province.DataSource = provinces;
            cb_province.DisplayMember = "name";  // Property to display in the ComboBox
            cb_province.ValueMember = "id";     // Property to use as the value
        }

        private async void cb_province_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Reset selected
            ResetCombobox(cb_district);
            ResetCombobox(cb_sale_office);
            // Get the selected item as a Province object
            if (cb_province.SelectedItem is Province selectedProvince)
            {
                try
                {
                    // Replace with your API URL
                    string apiUrl = $"https://shop-ctv.vnpt.vn/api/web/index.php/user/get-district-info?city_id={selectedProvince.id}";

                    // Fetch data as ApiResponse with a specific type for items
                    var response = await apiUrl
                        .WithHeader("x-api-key", "b7og0xhp1vbbuz9e9kbly4swwrsjp8qu")
                        .GetJsonAsync<ApiResponse<District>>();

                    // Access the items
                    List<District> districts = response.Data.items;

                    // Bind the ComboBox to the list
                    cb_district.DataSource = districts;
                    cb_district.DisplayMember = "name";  // Property to display in the ComboBox
                    cb_district.ValueMember = "id";     // Property to use as the value
                    cb_district.SelectedIndex = 0;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Lỗi khi lấy danh sách Quận / Huyện :" + ex.Message);
                }
            }
        }

        private async void cb_district_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Reset selected
            ResetCombobox(cb_sale_office);
            // Get the selected item as a Province object
            Console.WriteLine(cb_province.SelectedIndex);
            Console.WriteLine(cb_district.SelectedIndex);
            Console.WriteLine(!string.IsNullOrEmpty(DIGI_TOKEN));
            if (cb_province.SelectedItem is Province selectedProvince && cb_district.SelectedItem is District selectedDistrict && !string.IsNullOrEmpty(DIGI_TOKEN))
            {
                try
                {
                    // Replace with your API URL
                    string apiUrl = $"https://shop-ctv.vnpt.vn/api/web/index.php/sim-so/phong-giao-dich?city_id={selectedProvince.id}&district_id={selectedDistrict.id}";

                    // Fetch data as ApiResponse with a specific type for items
                    var response = await apiUrl
                        .WithHeader("x-api-key", "b7og0xhp1vbbuz9e9kbly4swwrsjp8qu")
                        .WithHeader("Authorization", "Bearer " + DIGI_TOKEN)
                        .GetJsonAsync<ApiResponse2<SaleOffice>>();

                    if (response != null && response.statusCode == 401)
                    {
                        MessageBox.Show("Lỗi khi lấy danh sách PGD. Vui lòng đăng nhập lại");
                        return;
                    }

                    // Access the items
                    List<SaleOffice> saleOffices = response.data;

                    // Bind the ComboBox to the list
                    cb_sale_office.DataSource = saleOffices;
                    cb_sale_office.DisplayMember = "ten";  // Property to display in the ComboBox
                    cb_sale_office.ValueMember = "id";     // Property to use as the value
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Lỗi khi lấy danh sách PGD :" + ex.Message);
                }

            }
        }

        private async void button1_Click_1(object sender, EventArgs e)
        {
            string username = tb_username.Text;
            string password = tb_password.Text;
            string[] lines = tb_dang_so.Text.Split(
                new string[] { Environment.NewLine },
                StringSplitOptions.RemoveEmptyEntries
            );

            digiClient = new HttpClient(proxyHandler);
            digiClient.DefaultRequestHeaders.Add("x-api-key", "b7og0xhp1vbbuz9e9kbly4swwrsjp8qu");
            var loginResponse = await callPostThread(digiClient, string.Format("https://shop-ctv.vnpt.vn/api/web/index.php/user/login?username={0}&password={1}", username, password), "");
            string token = getTokenLoginDigi(loginResponse);
            if (string.IsNullOrEmpty(token))
            {
                tb_log.Text = string.Format("Loi dang nhap, username: {0}. password: {1}.", username, password);
                return;
            }
            else
            {
                tb_log.AppendText(string.Format("Dang nhap thanh cong, username: {0}. password: {1}.", username, password));
                tb_log.AppendText(Environment.NewLine);
                DIGI_TOKEN = token;
            }
            digiClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
        }
    }
}
